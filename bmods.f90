!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE precision

! KIND values for single and double precision real variables

! Bart Willems, 16/10/00

      IMPLICIT NONE

      INTEGER, PARAMETER :: single = KIND(0.0)
      INTEGER, PARAMETER :: double = KIND(0.D0)

      END MODULE precision
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE consts

! Bart Willems, 06/05/01

      USE precision
      IMPLICIT NONE

! maxstep is the maximum number of time steps used to evolve the binary
! gn is the Newtonian gravitational constant in Rsun^3 Msun^{-1} yr^{-2}
      INTEGER, PARAMETER :: maxstep = 250000
      DOUBLE PRECISION, PARAMETER :: pi = 3.141592654d0
      DOUBLE PRECISION, PARAMETER :: TWOPI = 6.28318531E0
      DOUBLE PRECISION, PARAMETER :: RAD = 0.0174532925E0
      DOUBLE PRECISION, PARAMETER :: DEG2RAD = 0.0174532925
      DOUBLE PRECISION, PARAMETER :: gn = 6.67384d-11*5.7195044d18
      DOUBLE PRECISION, PARAMETER :: ln10 = 2.302585d0
      !Visual brightness of the sun
      !Defined in http://adsabs.harvard.edu/abs/2010AJ....140.1158T
      DOUBLE PRECISION, PARAMETER :: Vsun=-26.76
      DOUBLE PRECISION, PARAMETER :: teffSun=5777.d0,solarToCGS=6.993328363d-5
      DOUBLE PRECISION, PARAMETER :: Msun=1.98892d30,Rsun=6.955d8
      !Defined as Vsun+31.572+BC_v,sun derive BC from tables and use that
      DOUBLE PRECISION, PARAMETER :: BoloMagSun=4.75
      DOUBLE PRECISION, PARAMETER :: NewtGrav=6.67384d-11

      DOUBLE PRECISION,PARAMETER :: Rjup=0.100519051,TeffJup=160.d0
      END MODULE consts
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE params

! Bart Willems, 27/05/01

      USE precision
      IMPLICIT NONE
      SAVE

! iseed is the seeding factor for the random number generator
      INTEGER :: iseed = 4

! Maximum evolution time
	DOUBLE PRECISION :: tphysMax


! mflag > 0 activates mass loss
! tflag > 0 activates tides
! cflag > 0 sets non-conservative mass transfer
! lflag > 0 activates Dewi & Tauris (2000) treatment of CE
! ewd, ens, ebh > 0 allows super-Eddington accretion during RLOF
! for white dwarfs, neutron stars, and black holes, respectively
      INTEGER :: mflag,tflag,cflag,lflag
      INTEGER :: ewd,ens,ebh

! neta is the Reimers mass-loss coefficient
! bwind is the binary enhanced mass-loss parameter
! beta is the wind-velocity parameter for mass accretion
! wmag is an overall multiplication factor for all wind mass-loss rates
      DOUBLE PRECISION :: neta,bwind,beta,wmag

! gamma is the fraction of the mass lost from the system during RLOF
! nu measures the specific angular momentum lost during RLOF
! gnov is the fraction of the transferred mass accreted by a WD
!    in a nova-type system
! 1-gwd, 1-gns, 1-gbh are the fractions of the transferred mass accreted by
!    a WD not in a nova-type system, a neutron star (gns < 0 lets the
!    program determine the mass accretion rate based on the maximum mass
!    a NS is allowed to accrete), and a black hole
! wdsal and wdsah are the lower and upper limits for the mass transfer
!    to induce steady burning on the surface of a WD
! dnsm is the maximum mass a NS is allowed to accrete during RLOF
      DOUBLE PRECISION :: gamma,nu
      DOUBLE PRECISION :: gnov,gwd,gns,gbh
      DOUBLE PRECISION :: wdsal,wdsah,dnsm

! xlmin is the minimum xray luminosity (in Lsun) a binary needs to emit
! to be classified as an X-ray binary
! xlcrm is an estimated upper limit for the coronal X-ray luminosity for
! stars up to the giant branch
! mcnv is the minimum mass of the convective envelope as a fraction of the
! total mass in order for the corona to be X-ray active
      DOUBLE PRECISION :: xlmin,xlcrm
      DOUBLE PRECISION :: mcnv

! WASP line: mdmv is minimum decrease in magnitude required to detect transit
!            mmv1 is minimum magnitude detectable
!            mmv2 is maximum magnitude detectable
      DOUBLE PRECISION :: mdmv,mmv1,mmv2

! ace is the efficiency parameter during common envelope evolution
! lambda is the binding energy parameter for a common envelope phase
      DOUBLE PRECISION :: ace,lambda

!
! etamb is an ad-hoc parameter to change the MB strength
      DOUBLE PRECISION :: etamb

! nkick is the number of different kicks to compute after a SN explosion
! ksig is the kick velocity dispersion (for a Maxwellian distribution)
      INTEGER :: nkick
      DOUBLE PRECISION :: ksig

! pts1,pts2,pts3,pts4 determine the timesteps chosen to evolve the binary
      DOUBLE PRECISION :: pts1,pts2,pts3,pts4

! Do we want transits recorded or not?
!Are we looking at single or bianry systems
	INTEGER :: wantTransit,isSingle,numTransits
	CHARACTER(len=2) :: filterBand

	DOUBLE PRECISION :: metallicity

      END MODULE params
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE dtypes

! Bart Willems, 23/03/01

      USE precision
      USE params
      IMPLICIT NONE


      
!       TYPE :: spectralType
!       	CHARACTER(len=4) :: class,subClass,Lumin
!       END TYPE spectralType

      TYPE :: stq                    ! Stellar parameters
         INTEGER :: kw               ! Stellar type
         DOUBLE PRECISION :: mass   ! Initial mass
         DOUBLE PRECISION :: mt     ! Current mass
         DOUBLE PRECISION :: lum    ! Luminosity
         DOUBLE PRECISION :: r      ! Radius
         DOUBLE PRECISION :: mc     ! Core mass
         DOUBLE PRECISION :: rc     ! Core radius
         DOUBLE PRECISION :: reff   ! Effective Radius
         DOUBLE PRECISION :: tphys  ! Time
         DOUBLE PRECISION :: epoch  ! Start of current evolutionary phas
         DOUBLE PRECISION :: ospin  ! Rotational angular velocity
         DOUBLE PRECISION :: jspin  ! Rotational angular momentum
         DOUBLE PRECISION :: djmb   ! djspin/dt due to magnetic braking
         DOUBLE PRECISION :: rl     ! Radius of the Roche lobe
         DOUBLE PRECISION :: dmw    ! Wind mass loss rate
         DOUBLE PRECISION :: dma    ! Wind accretion rate
         DOUBLE PRECISION :: xlw    ! Wind accretion luminosity
         DOUBLE PRECISION :: djmw   ! djspin/dt due to wind mass loss
         DOUBLE PRECISION :: djma   ! djspin/dt due to wind accretion
         DOUBLE PRECISION :: dmrl   ! Mass trasfer rate due to RLOF
         DOUBLE PRECISION :: xldm   ! Mass transfer accretion luminosity
         DOUBLE PRECISION :: xlcor  ! Coronal X-ray luminosity
         DOUBLE PRECISION :: teff   ! Effective Temperature
          DOUBLE PRECISION :: ldc1,ldc2 ! Limb Darkening parameters
          DOUBLE PRECISION :: mag 	!Mag of star in (filterBand)
         !Magnitudes in each 
!          TYPE(spectralType) :: specType ! Spectral type of star
          DOUBLE PRECISION :: grav ! Gravity darkening coefficent
      END TYPE stq

      TYPE :: bnq                    ! Binary parameters
         INTEGER :: kw               ! Binary type
         INTEGER :: cls              ! Binary class
         DOUBLE PRECISION :: Porb   ! Orbital period
         DOUBLE PRECISION :: a      ! Semi-major axis
         DOUBLE PRECISION :: ecc    ! Eccentricity
         DOUBLE PRECISION :: jorb   ! Orbital angular momentum
         DOUBLE PRECISION :: dagr   ! Gravitational radiation => da/dt
         DOUBLE PRECISION :: degr   ! Gravitational radiation => de/dt
         DOUBLE PRECISION, DIMENSION(2) :: damw ! Wind loss/accr => da/dt
         DOUBLE PRECISION, DIMENSION(2) :: demw ! Wind loss/accr => de/dt
         DOUBLE PRECISION :: number ! Sequence number
          DOUBLE PRECISION :: mag    !Mag of binary in (filterBand)
         DOUBLE PRECISION :: lum1,lum2 ! Luminsoities of both stars
      END TYPE bnq

      END MODULE dtypes
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE dshare

! Bart Willems, 18/09/01

      USE precision
      USE params
      IMPLICIT NONE
      SAVE

      TYPE :: summary
         INTEGER :: kw1               ! Stellar type star1
         INTEGER :: kw2               ! Stellar type star2
         INTEGER :: bkw               ! Binary type
         INTEGER :: cls               ! Binary classification
         DOUBLE PRECISION :: tphys   ! Time
         DOUBLE PRECISION :: mass1   ! Initial mass star1
         DOUBLE PRECISION :: mass2   ! Initial mass star2
         DOUBLE PRECISION :: mt1     ! Current mass star1
         DOUBLE PRECISION :: mt2     ! Current mass star2
         DOUBLE PRECISION :: reff1   ! Effective radius star1
         DOUBLE PRECISION :: reff2   ! Effective radius star2
         DOUBLE PRECISION :: epoch1  ! Epoch star1
         DOUBLE PRECISION :: epoch2  ! Epoch star2
         DOUBLE PRECISION :: Porb    ! Orbital period
         DOUBLE PRECISION :: ecc     ! Eccentricity
         DOUBLE PRECISION :: num     ! Sequence number
         DOUBLE PRECISION :: lum1    ! Luminosity star1
         DOUBLE PRECISION :: lum2    ! Luminosity star2
         DOUBLE PRECISION :: teff1   ! Effective temperature star1
         DOUBLE PRECISION :: teff2   ! Effective temperature star2
         DOUBLE PRECISION :: xlw1    ! Wind X-ray luminosity star1
         DOUBLE PRECISION :: xlw2    ! Wind X-ray luminosity star2
         DOUBLE PRECISION :: dm1     ! Mass-transfer rate star1
         DOUBLE PRECISION :: dm2     ! Mass-transfer rate star2
         DOUBLE PRECISION :: xldm1   ! Mass transfer luminosity star1
         DOUBLE PRECISION :: xldm2   ! Mass transfer luminosity star2
!          DOUBLE PRECISION :: bc1     ! Bolometric correction star1
!          DOUBLE PRECISION :: bc2     ! Bolometric correction star2
!          DOUBLE PRECISION :: dmin    ! min observable distance for given
                                      ! magnitude limit
!          DOUBLE PRECISION :: dmax    ! max observable distance for given
                                      ! magnitude limit
!          DOUBLE PRECISION :: mag	  !abs Magnitude of system
!          DOUBLE PRECISION :: mag1,mag2    !Mags of each star

		DOUBLE PRECISION :: m1min,m1max,m2min,m2max,amin,amax !intial grids
		DOUBLE PRECISION :: emin,emax
		INTEGER :: m1bin,m2bin,abin,ebin
      END TYPE summary

      INTEGER, PARAMETER :: nlog = 5000
      INTEGER :: warn,jp,nnlog
      INTEGER :: kkick,kdisr,kmerg
      DOUBLE PRECISION :: ksnii
      DOUBLE PRECISION :: tsave
      TYPE(summary), ALLOCATABLE, DIMENSION(:) :: blog
      LOGICAL :: dsave,asave

      END MODULE dshare
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE error
	contains
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE setError()

!Write to stderr a error code, can then pipe the stderr to a file
!Can then check for existence of stderr file to see if code exited
!prematurely
	write(0,*) "1"
	return

	END SUBROUTINE setError

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
END MODULE error

#!/bin/bash


# Absolute path to this script, 
THIS_SCRIPT=$(readlink -f "$0")

# Absolute directory this script is in, 
THIS_DIR=$(dirname "$THIS_SCRIPT")



# remove any old versions
make -f "$THIS_DIR/makefile_transit_calc" clobber


# compile new version
make -f "$THIS_DIR/makefile_transit_calc"

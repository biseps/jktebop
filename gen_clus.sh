#!/bin/bash

#$ -N jktebop_transits

#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=18G

# 49 extract sub-files for pop_b_o
# 52 extract sub-files for pop_b_y
#$ -t 52-52:1

# sun grid engine, email options: 
# (b)egun
# (e)nded
# (a)borted 
# (s)uspended

#$ -M enda.farrell@open.ac.uk
#$ -m ase

#$ -S /bin/bash

#$ -j y

#$ -cwd



# get utility functions.
# This file is important and contains 
# bash functions used throughout this script.
source "$CODE_DIR/utils/utility_functions.sh"


# Setup the Python runtime env
set_python_env  anaconda  $CODE_DIR


# pad SGE_TASK_ID with zeros
# this is needed to grab the correct input file
TASK_ID=$(printf "%0.2d" "$SGE_TASK_ID")
echo "task_id is: $TASK_ID"


# input/output data files 
# extract_file= $INPUT_DIR/extract.$VER
EXTRACT_FILE="$INPUT_DIR/extractsmall.$TASK_ID"
COEFF_FILE="$OUTPUT_DIR/coeff.$TASK_ID"
DEPTHS_FILE="$OUTPUT_DIR/transit_depths.dat.$TASK_ID"


# create gravity and limb darkening coefficients

echo "starting...  magLdcGd.py"
$PYTHONBIN $CODE_DIR/mag/magLdcGd.py  $EXTRACT_FILE  > $COEFF_FILE
echo "magLdcGd.py finished"


# Get transit depths using JKTEBOP

echo "starting...  jktebopGen"
$OUTPUT_DIR/jktebopGen  $EXTRACT_FILE  $COEFF_FILE   > "$DEPTHS_FILE"
echo "jktebopGen finished"


PROGRAM jktebopGen

    USE jktebop
    USE dtypes
    USE consts
    USE randomNum
    USE csv_file

    IMPLICIT NONE

    integer, parameter :: STDERR = 0 
    integer, parameter :: STDIN  = 5 
    integer, parameter :: STDOUT = 6 


    TYPE(stq),DIMENSION(1:2):: s
    TYPE(bnq) :: b
    INTEGER :: i, angle, j, tmpInt, lineCount, mt, kw1, kw2, bkw, pix, isSat
    DOUBLE PRECISION :: Z, id
    DOUBLE PRECISION :: alpha1, beta1, gamma1, delta1, eta1
    DOUBLE PRECISION :: birth, death, num, tmpFloat
    DOUBLE PRECISION :: ccdnoise, bgflux, signal, noise, oscill
    DOUBLE PRECISION, PARAMETER :: dis=10.d0
    CHARACTER(len=200) :: extract_file, coeff_file, fin3, fin4, fileOut, outFolder
    CHARACTER(len=1000) :: BUFFER
    LOGICAL :: fileExists, rerun
    DOUBLE PRECISION :: critical_inclin
    TYPE(ext) :: extra
    DOUBLE PRECISION, DIMENSION(2, 3) :: transDepth
    CHARACTER(LEN=30)  :: output_format

    !Files hosuld be from targetEB.py
    
    !extract file
    call GETARG(1, extract_file)

    !coeff file
    call GETARG(2, coeff_file)

    !output folder
    call GETARG(3, outFolder)


    extract_file = extract_file(1:len_trim(extract_file))
    coeff_file   = coeff_file(1:len_trim(coeff_file))

    outFolder    = outFolder(1:len_trim(outFolder))
    fileOut      = 'lightcurve,dat'

    CALL preIntRan()


    fileExists = .false.
    inquire(FILE=extract_file, exist=fileExists)
    if(fileExists .eqv. .false.) then
        write(0, *) "No extract file ", extract_file
        stop
    end if


    fileExists = .false.
    inquire(FILE=coeff_file, exist=fileExists)
    if(fileExists .eqv. .false.) then
        write(0, *) "No coeff file ",  coeff_file
        stop
    end if

    !OPEN(2, FILE=coeff_file, STATUS='OLD', ACTION='READ')


    OPEN(1, FILE=extract_file, STATUS='OLD', ACTION='READ')

    lineCount = 0
    DO WHILE (.true.)
        read(1,  '(A)',  end=99) BUFFER
        lineCount=lineCount+1
      ENDDO
99    CONTINUE
      CLOSE(1)

    j = 1


    OPEN(10, file=extract_file, STATUS='OLD', ACTION='READ')
    OPEN(11, file=coeff_file,   STATUS='OLD', ACTION='READ')


    DO i=1, lineCount
        !leave doesnt need to be set to the actual distance just to a value
        extra%dis=10.d0

                    

        !read extract file

        READ(10, *) num, id, &
                    s(1)%mt, s(1)%reff, s(1)%teff, s(1)%lum, &
                    s(2)%mt, s(2)%reff, s(2)%teff, s(2)%lum, &
                    b%porb, birth, death, mt, kw1, kw2, bkw, Z, BUFFER

        !read limb darkening and gravity darkening file

        READ(11, *) s(1)%mag, s(1)%ldc1, s(1)%ldc2, s(1)%grav, &
                    s(2)%mag, s(2)%ldc1, s(2)%ldc2, s(2)%grav
    
    
        ! Set noise level 
        ! only used for the output light curve 
        ! not the transit depth calc
        extra%noise = 0.d0

        ! background lught contribution, is used in transit depth calc, 
        ! should be in units normalised to the flux of the binary
        extra%thirdLight = 0.d0
        
        b%a = porb2a(b%porb/365.0, s(1)%mt, s(2)%mt)

        ! Generate random inclcination in range 0-pi/2
        ! extra%inclin = unif(0.d0, pi/2.d0)

        ! create inclination between 80 and 90 degrees
        ! extra%inclin = unif(pi/2.25, pi/2.d0)

        critical_inclin = acos((s(1)%reff + s(2)%reff) / b%a)


        ! calculate a transit depth
        ! for all inclination angles above critical angle
        DO angle = int(critical_inclin), 90

            extra%inclin = DBLE(angle * pi/180)

            ! Set eccentricity and omega if needed
            extra%e = 0.d0
            extra%w = 0.d0
            extra%ecosw = extra%e*cos(extra%w)
            extra%esinw = extra%e*sin(extra%w)

                
            transDepth = 0.d0

            ! adding a "!" means write no data out
            ! Set mass to negative so we have no ellispdidal modulation 
            ! for purpose of calculating transit depth
            s(2)%mt = -s(2)%mt

            ! write(*,*) "just before calling jkt"
            CALL jkt(s(1), s(2), b, extra, '!', transDepth(1,:))
            CALL jkt(s(2), s(1), b, extra, '!', transDepth(2,:))
                    
                    
            ! output data format :
            ! id, porb, inclinc, critical inclin,  
            ! trans depth 1, ellipd epth 1,  
            ! trans depth2, ellip depth 2, noise

            if(extra%inclin < critical_inclin) THEN


                !non-eclisping systems may have elliposidal varaitions

                ! write(*,*) id, &
                           ! b%porb, &
                           ! extra%inclin * 180.d0/pi, &
                           ! critical_inclin * 180.d0/pi, &
                           ! 0.d0, &
                           ! transDepth(1, 2)-transDepth(1, 3), &
                           ! 0.d0, &
                           ! transDepth(2, 2)-transDepth(2, 3), &
                            ! extra%noise

            ELSE

                !eclisping signals
                ! output_format = "(F21.11, F15.5, 8(F10.5))"
                ! output_format = '(F21.11,  9(",", F15.5))'

                call csv_write(STDOUT, num, advance=.false.)
                call csv_write(STDOUT, id, advance=.false.)
                call csv_write(STDOUT, b%porb, advance=.false.)
                call csv_write(STDOUT, extra%inclin * 180.d0/pi, advance=.false.)
                call csv_write(STDOUT, critical_inclin * 180.d0/pi, advance=.false.)
                call csv_write(STDOUT, transDepth(1,3) - transDepth(1,1), advance=.false.)
                call csv_write(STDOUT, transDepth(1,2) - transDepth(1,3), advance=.false.)
                call csv_write(STDOUT, transDepth(2,3) - transDepth(2,1), advance=.false.)
                call csv_write(STDOUT, transDepth(2,2) - transDepth(2,3), advance=.false.)
                call csv_write(STDOUT, extra%noise, advance=.false.) 
                call csv_write(STDOUT, Z, advance=.true.) 

            END IF
            
            
            !reset mass         
            s(2)%mt = abs(s(2)%mt)

            !call to generate an output light curve
            ! CALL jkt(s(1), s(2), b, extra, trim(adjustl(fileOut)), transDepth(1,:))
        END DO

    END DO  


END PROGRAM jktebopGen

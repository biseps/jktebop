#!/bin/bash

# Purpose:

# Load all eclipse depths files (created by 'calc_depths.sh')
# into an SQL relational database.

# This allows the eclipse depth for any
# binary system to be quickly accessed.


#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G
#$ -t 1-1:1
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -P science
#$ -M pam.rowden@open.ac.uk
#$ -m as


# For debugging:
CODE_DIR="/padata/beta/users/pmr257/repos/biseps"
# CODE_DIR='../../code'
OUTPUT_SQUARE_DIR="/padata/stfc2/pmr257/data/plato/jkt_output/pop_37/49"
# OUTPUT_SQUARE_DIR="/padata/beta/users/pmr257/data/plato/jktebop_cluster/output/run_75/95"
GRID_SQUARE='49'
# BINARY_TYPE='pop_b_o'
BINARY_TYPE='pop_b_y'

PROGRAM_SQLITE="/padata/stfc2/pmr257/data/plato/jkt_output/run_15/sqlite3"



all_eclipses_file="$OUTPUT_SQUARE_DIR/all_eclipses.$BINARY_TYPE.dat"

csv_separator=","

db_name="$OUTPUT_SQUARE_DIR/depths_$GRID_SQUARE.sqlite"

table_name='depths'

columns="( num                    real,
           id                     real,
           porb                   real,
           inclin                 real,
           critical_inclin        real,
           trans_depth1           real,
           ellip_depth1           real,
           trans_depth2           real,
           ellip_depth2           real,
           noise                  real,
           z                      real
           )"


# list all eclipse depth files 
# that have been created by the previous job
/bin/ls  $OUTPUT_SQUARE_DIR/eclipse_depth.${BINARY_TYPE}.*


# combine eclipse depths 
# into one big file
/bin/cat $OUTPUT_SQUARE_DIR/eclipse_depth.${BINARY_TYPE}.* > "$all_eclipses_file"



# create SQLITE table 
# to hold eclipse depths
$PROGRAM_SQLITE  "$db_name" <<EOF
CREATE TABLE IF NOT EXISTS ${table_name} ${columns};
EOF



# load/import data 
# into SQLITE database
$PROGRAM_SQLITE  "$db_name" <<EOF
.echo on
.separator "${csv_separator}"

begin transaction;
.import "${all_eclipses_file}" ${table_name}
commit;

select count(*) from ${table_name};
EOF

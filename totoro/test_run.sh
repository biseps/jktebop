#!/bin/bash

# setup input variables
VERSION=1


# setup paths
CODE_DIR="/padata/beta/users/pmr257/repos/biseps"
INPUT_DIR="/padata/stfc2/pmr257/data/plato/pop_output/field1/run_37/pop_b_y/49"
OUTPUT_DIR="/padata/stfc2/pmr257/data/plato/jkt_output/pop_37/49"


# Create output folder if
# it doesnt exist already
if [ ! -d "$OUTPUT_DIR" ]; then
    mkdir "$OUTPUT_DIR"
fi


# get utility functions.
# This file is important and contains 
# bash functions used throughout this script.
source "$CODE_DIR/common/utility_functions.sh"


# Setup Python runtime environment
set_python_env  anaconda  $CODE_DIR



# input files
# EXTRACT_FILE="$INPUT_DIR/extract.onefile"
EXTRACT_FILE="$INPUT_DIR/extract.$VERSION"

# output files
COEFF_FILE="$OUTPUT_DIR/coeff.pop_b_y.000"
DEPTHS_FILE="$OUTPUT_DIR/eclipse_depth.pop_b_y.000"


# check that required folders and files exist
assert_folder_exists    "OUTPUT_DIR"    "$OUTPUT_DIR"
assert_file_exists      "EXTRACT_FILE"  "$EXTRACT_FILE"


# First create gravity and limb 
# darkening coefficients
printf "\nstarting...  magLdcGd.py"
$PYTHONBIN $CODE_DIR/mag/magLdcGd.py  $EXTRACT_FILE  > "$COEFF_FILE"
printf "\nmagLdcGd.py finished"


# Now double-check a coefficient 
# file was actually created
assert_file_exists  "COEFF_FILE"    "$COEFF_FILE"


# Finally get transit depths using JKTEBOP
printf "\n\nstarting...  jktebopGen"
$CODE_DIR/jktebop/jktebopGen  $EXTRACT_FILE  $COEFF_FILE   > "$DEPTHS_FILE"
printf "\njktebopGen finished"



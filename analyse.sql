
create index idx_popbo_inclin on pop_b_o (inclin);
create index idx_popbo_transdepth1 on pop_b_o (trans_depth1);
create index idx_popby_transdepth1 on pop_b_y(trans_depth1);
create index idx_popby_inclin on pop_b_y(inclin);

select count(*) from  pop_b_o ;
select count(*) from  pop_b_y ;


select * from pop_b_o limit 5;

select count(*) from  pop_b_o where trans_depth1 > 0.2;
select count(*) from  pop_b_o where trans_depth1 < 1;
select count(*) from  pop_b_o where trans_depth1 < .8;
select count(*) from  pop_b_o where trans_depth1 < .6;
select count(*) from  pop_b_o where trans_depth1 < .4;

select count(*) from pop_b_o where trans_depth1 > 0.001 and trans_depth1 < 0.01;

select avg(trans_depth1) from pop_b_o where trans_depth1 > 0.001 and trans_depth1 < 0.01;
select avg(trans_depth1) from pop_b_o where trans_depth1 > 0.2;


select pop_b_o.id from pop_b_o, pop_b_y where pop_b_o.id = pop_b_y.id limit 10;

select * from pop_b_o where id = 9430.0;

select * from pop_b_y where id = 9430.0;

select count(*) from pop_b_o where id = 9430.0;
select count(*) from pop_b_y where id = 9430.0;


MODULE jktebop
!This is adapted code form John Southworths JKTEBOP binary simulator
! avaible at http://www.astro.keele.ac.uk/~jkt/codes/jktebop.html
! References:
! Popper & Etzel (1981AJ.....86..102)
! Etzel (1981psbs.conf..111E)
! Nelson & Davis (1972ApJ...174..617N)
! Southworth et al. (2004MNRAS.351. 1277S)
! Southworth et al. (2004MNRAS.355..986S)
!     *  Southworth et al. (2005MNRAS.363..529S): shows that the Monte Carlo analysis gives reliable results
!     * Bruntt et al. (2006A&A...456..651B): fitting for orbital period and time of minimum light
!     * Southworth et al. (2007A&A...467.1215S): non-linear limb darkening, and inclusion of spectroscopic light ratios and times of minimum light as observations
!     * Southworth et al. (2007MNRAS.379L..11S): linear limb darkening is inadequate for the fitting the HST photometry of the transiting extrasolar planetary system HD 209458
!     * Southworth (2008MNRAS.386.1644S): correlated noise algorithm, cubic-law limb darkening, and a detailed investigation of fourteen transiting extrasolar planetary systems using JKTEBOP
!     * Southworth et al. (2009ApJ...707..167S): inclusion of ecosω and esinω as fitted parameters
!     * Southworth (2010MNRAS.408.1689S): specification of measured values of third light


!-----------------------------------------------------------------------

! V(1)  = surface brightness ratio      
! V(2)  = sum of fractional radii       
! V(3)  = ratio of stellar radii        
! V(4)  = linear LD for star A          
! V(5)  = linear LD for star B          
! V(6)  = orbital inclination           
! V(7)  = e cos(omega) OR ecentricity   
! V(8)  = e sin(omega) OR omega         
! V(9)  = gravity darkening 1           
! V(10) = gravity darkening 2          
! V(11) = primary reflected light      
! V(12) = secondary reflected light    
! V(13) = stellar mass ratio           
! V(14) = tidal lead/lag angle (deg)   
! V(15) = third light               
! V(16) = phase correction          
! V(17) = light scaling factor      
! V(18) = integration ring size (o) 
! V(19) = orbital period (days)     
! V(20) = ephemeris timebase (days) 
! V(21) = nonlinear LD for star A   
! V(22) = nonlinear LD for star B   

! VEXTRA(1) = primary star radius   
! VEXTRA(2) = secondary star radius 
! VEXTRA(3) = stellar light ratio   
! VEXTRA(4) = eccentricity          
! VEXTRA(5) = periastron longitude  
! VEXTRA(6) = reduced chi-squared   

! V(68)     = orbital speration in solar radii
! V(23-37)  = five lots of sine curve [T0,P,amplitude]
! V(38-67)  = five lots of polynomial [pivot,x,x2,x3,x4,x5] 

!-----------------------------------------------------------------------

	IMPLICIT NONE
	DOUBLE PRECISION,DIMENSION(1:70) :: V
	
	TYPE :: ext
		DOUBLE PRECISION :: inclin,e,w,ecosw,esinw,dis
		DOUBLE PRECISION :: noise,thirdLight
		DOUBLE PRECISION :: dep1,dep2,ellip1,ellip2
	ENDTYPE ext


	CONTAINS



	SUBROUTINE jkt(s1, s2, b, extra, fileOut, output)

      USE dtypes
      USE consts
      implicit none

      INTEGER :: LDTYPE(2), PSINE(5), PPOLY(5)
      INTEGER :: NSINE, NPOLY
      CHARACTER(len=*), INTENT(IN) :: fileOut

      TYPE (stq), INTENT(IN) :: s1,s2
      TYPE (bnq), INTENT(IN) :: b
      TYPE(ext),INTENT(INOUT) :: extra
      DOUBLE PRECISION,INTENT(OUT),DIMENSION(1:3) :: output
    
      PSINE = 0
      PPOLY = 0
      NSINE = 0
      NPOLY = 0

      ! assume quadratic Limb darkening
      ! for each star
      LDTYPE(1) = 4
      LDTYPE(2) = 4
    
      ! assume star is 1 primary
      V=0.d0

      ! old method
      ! ! compute surface brightness ratio, maybe simpliflable?
      ! V(1) = (s2%mag + 2.5*log10((pi*s2%reff**2)/(44334448.006896*extra%dis)))/ &
             ! (s1%mag + 2.5*log10((pi*s1%reff**2)/(44334448.006896*extra%dis)))


      ! E Farrell May 2014
      ! New method advised by Rob
      ! compute surface brightness ratio
      V(1) = 10**(((s1%mag - s2%mag) + 5.0*log10(s1%reff/s2%reff)) / 2.5)

      V(2) = (s1%reff + s2%reff) / b%a
      V(3) = s2%reff / s1%reff
      V(4) = s1%ldc1
      V(5) = s2%ldc1
      V(6) = extra%inclin * 180.d0/pi
      V(7) = extra%ecosw
      V(8) = extra%esinw

      !Gravity Darkening 
      V(9)  = s1%grav
      V(10) = s2%grav

      !Gets set in TASK2 so dont worry
      V(11) = 0.d0;
      V(12) = 0.d0

      V(13) = s2%mt/s1%mt
      V(14) = 0.d0
      V(15) = extra%thirdLight

      !Must stay at 0 phase shift
      V(16) = 0.0
      V(17) = 0.d0
      V(18) = 1.0d0
      V(19) = b%Porb
      V(20) = 0.d0
      V(21) = s1%ldc2
      V(22) = s2%ldc2
      V(68) = b%a
      v(69) = s1%reff
      V(70) = s2%reff


      ! write(*,*) "just before calling task2"
      CALL TASK2(LDTYPE, NSINE, PSINE, NPOLY, PPolY, fileOut, extra, output)

	END SUBROUTINE jkt




!         SUBROUTINE TASK2 (V,LDTYPE,NSINE,PSINE,NPOLY,PPOLY,output)
	SUBROUTINE TASK2 (LDTYPE, NSINE, PSINE, NPOLY, PPolY, fileOut, extra, output)

        USE consts
        USE randomNum
        implicit none                       

        ! Produces a model light curve
        ! IN: light  curve  parameters
        ! DOUBLE PRECISION, INTENT(INOUT) :: V(68)                        

        ! IN: LD law type for each star
        INTEGER, INTENT(IN) :: LDTYPE(2)                   

        ! LOCAL: counters & error flag
        INTEGER :: i, ERROR, aerr, m                     

        ! LOCAL: EBOP/GETMODEL  output
        DOUBLE PRECISION :: LS, LP                  

        ! LOCAL:  phase for evaluation
        DOUBLE PRECISION :: PHASE                        

        ! FUNCTION: evaluate the model
        ! DOUBLE PRECISION :: GETMODEL                     

        ! OUT: Numbrs of sines and L3s
        INTEGER, INTENT(INOUT) :: NSINE                       


        ! OUT: Which par for each sine
        INTEGER, INTENT(INOUT) :: PSINE(5)                  


        ! OUT: Similar for polynomials
        INTEGER, INTENT(INOUT) :: NPOLY, PPOLY(5)              


        DOUBLE PRECISION :: HJD, magNew, magOld, magS
        DOUBLE PRECISION :: R1, R2, minChange


        ! LOCAL: number of phases todo
        INTEGER, PARAMETER :: NPHASE =6001                

        DOUBLE PRECISION, DIMENSION(NPHASE) :: MAG, MAG2, sig
        ! DOUBLE PRECISION, DIMENSION(1:3), INTENT(OUT) :: output

        DOUBLE PRECISION :: RPA, RPB, EP, RP, ES, RSA, RSB, RS, q, in, minV

        CHARACTER(len=*), INTENT(IN) :: fileOut
        TYPE(ext), INTENT(INOUT) :: extra
        DOUBLE PRECISION, INTENT(OUT), DIMENSION(1:3) :: output


        MAG    = 0.d0
        MAG2   = 0.d0
        sig    = 0.d0
        OUTPUT = 0.d0

        ! Set period to 1.0
        V(19)  = 1.0d0           

        ! Set Tzero to 0.0
        V(20)  = 0.0d0           

        LP = 0.0d0
        LS = 0.0d0


        ! NSINE=0 and NPOLY=0 and NUMINT=1
        if ( V(2) >= 0.0d0 ) then
            R1 = V(2) / (1.0d0 + V(3))
            R2 = V(2) / (1.0d0 + (1.0d0/V(3)))
        else
            R1 = abs(V(2))
            R2 = V(3)
        end if


        !flux at zero phase is mid eclipse
        PHASE     = 0
        HJD       = V(20) + PHASE * V(19)
        MAGS      = GETMODEL(LDTYPE, 0, PSINE, 0, PPOLY, HJD, 1, LP, LS, 1, 0.0d0)
        output(1) = 10**(MAGS/(-2.5))


        !flux at 1/4 phase is due to ellispidoal varaition 
        phase     = 0.25
        HJD       = V(20) + PHASE * V(19)
        MAGS      = GETMODEL(LDTYPE, 0, PSINE, 0, PPOLY, HJD, 1, LP, LS, 1, 0.0d0)
        output(2) = 10**(MAGS/(-2.5))


        ! point of first contact
        if ( V(3) >= 0.0d0 ) then
            RP   = dble( V(2)/(1.0d0+V(3)) )
            RS   = dble( V(2)/(1.0d0+(1.0d0/V(3))) )
        else
            RP   = dble( V(2) )
            RS   = abs( dble( V(3) ) )
        end if


        Q = dble( V(13) )

        ! EBOP subroutine to calculate biaxial ellipsoid dimensions
        ! and oblateness for each star after Chandrasekhar (1933).
        CALL BIAX (RP, Q, RPA, RPB, EP)
        CALL BIAX (RS, 1.d0/Q, RSA, RSB, ES)


        in        = v(6) * pi/180.d0
        phase     = atan((sqrt(RPB**2+RPA**2) + sqrt(RSA**2+RSB**2)) / V(68)) / (pi)
        HJD       = V(20) + PHASE * V(19)
        MAGS      = GETMODEL(LDTYPE, 0, PSINE, 0, PPOLY, HJD, 1, LP, LS, 1, 0.0d0)
        output(3) = 10**(MAGS/(-2.5))


        ! Set period to 1.0
        V(19) = 1.0d0           

        ! Set Tzero to 0.0
        V(20) = 0.0d0           

        ! NSINE=0 and NPOLY=0 and NUMINT=1

        LP = 0.0d0
        LS = 0.0d0


        ! Now generate the whole light curve 
        ! (only need if ouputing a light curve) 
        ! with possible guassian noise added

        ! do i = 1, NPHASE
            ! PHASE   = (i-1) / dble(NPHASE-1)
            ! HJD     = V(20) + PHASE  * V(19)
            ! MAG(i)  = GETMODEL(LDTYPE, 0, PSINE, 0, PPOLY, HJD, 1, LP, LS, 1, 0.0d0)
        ! end do


        ! MAG = 10**(MAG/(-2.5))

        ! get minimum value in array
        ! m   = minloc(MAG, DIM=1)

        ! DO i=1, NPHASE
            ! MAG(i) = gaussDraw(MAG(i), extra%noise)
        ! END DO


        ! MAG2 = 0.d0

        ! Shifts the eclipses such that the primary 
        ! should be at 0 phase which is now in the 
        ! middle of the file (ie we ouput from -0.5 to 0.5 phase
        ! DO i=1, NPHASE
            ! MAG2(i) = MAG(MOD(NPHASE + FLOOR(NPHASE/2.0)-m+i, NPHASE) + 1)
        ! END DO


        ! write lightcurve out to file
        ! IF(index(fileOut, "!") == 0) THEN
            ! OPEN(50, FILE=trim(adjustl(fileOut)), STATUS='UNKNOWN', ACTION='WRITE')

            ! DO i=1, NPHASE
                ! write (50, '(F16.12)') MAG2(i)
            ! END DO

            ! CLOSE(50)
        ! END IF
		
		
  END SUBROUTINE TASK2




	DOUBLE PRECISION FUNCTION GETMODEL (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,DTYPE,LA,LB,NUMINT,NINTERVAL)

        ! Output a predicted model value according to the parameters
        ! in array V. Precise meaning of the value depends on DTYPE.

        ! DTYPE=1  it outputs an EBOP magnitude for given time
        ! DTYPE=2  it outputs a light ratio for the given time
        ! DTYPE=3  outputs a time of eclipse for the given =CYCLE=
        ! DTYPE=4  it simply outputs the third light value


        ! DTYPE=1 for light curve datapoint
        ! DTYPE=2 for light ratio
        ! DTYPE=3 for times of minimum light
        ! DTYPE=4 for third light
        ! DTYPE=5 for e*cos(omega)
        ! DTYPE=6 for e*sin(omega)

        implicit none
        !       real*8 V(68)                  ! IN: Photometric parameters
        integer LDTYPE(2)             ! IN: LD law type for the two stars
        real*8 TIME                   ! IN: The given TIME, PHASE or CYCLE
        integer DTYPE                 ! IN: 1-6 depending on wanted result
        integer NSINE,PSINE(5)        ! IN: number and parameters of sines
        integer NPOLY,PPOLY(5)        ! IN: number and parameters of polys
        integer NUMINT                ! IN: Number of numerical integratns
        real*8 NINTERVAL              ! IN: Time interval for integrations
        real*8 LA,LB                  ! OUT: Light produced by each star
        real*8 FMAG,LP,LS             ! LOCAL: LIGHT subroutine output
        real*8 ECC,OMEGA,ECOSW,ESINW  ! LOCAL: orbital shape parameters
        real*8 GETPHASE        ! FUNCTIONS
        real*8 FMAGSUM,LASUM,LBSUM
        integer i
        real*8 TIMEIN

        GETMODEL = 0.0d0


        if ( DTYPE == 1 ) then
            ! output EBOP magnitude for given time 
            ! i.e. lightcurve data point

            if ( NUMINT == 1 ) then
                CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)
                LA = LP
                LB = LS
                GETMODEL = FMAG

            else if ( NUMINT > 1 ) then
                FMAGSUM = 0.0d0
                LASUM = 0.0d0
                LBSUM = 0.0d0

                CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)

                do i = 1, NUMINT
                    TIMEIN  =  TIME  -  NINTERVAL / 86400.0d0 / dble(NUMINT) * &
                   ( dble(NUMINT) - 2.0d0*dble(i) + 1.0d0 ) / 2.0d0

                    CALL LIGHT(LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIMEIN,FMAG,LP,LS)
                    FMAGSUM = FMAGSUM + FMAG
                    LASUM = LASUM + LP
                    LBSUM = LBSUM + LS
                end do

                FMAG = FMAGSUM / NUMINT
                LA = LASUM / NUMINT
                LB = LBSUM / NUMINT
                GETMODEL = FMAG

            else
                write(6,*)"NUMINT is less than 1 in function GETMODEL. Abort."
                write(6,*)"NUMINT =    ", NUMINT
                write(6,*)"NINTERVAL = ", NINTERVAL
                stop
            end if


        else if ( DTYPE == 2 ) then
            ! output a light ratio for the given time
            CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)
            GETMODEL = LS / LP


        else if ( DTYPE == 3 ) then
            ! outputs a time of eclipse for the given =CYCLE=
            ! (times of minimum light)
            if ( V(7) > 5.0d0 ) then
              ECC = V(7) - 10.0d0
              OMEGA = V(8)
            else
              ECC = sqrt(V(7)**2 + V(8)**2)
              OMEGA = atan2(V(8),V(7)) * 45.0d0 / atan(1.0d0)
              if ( OMEGA < 0.0d0 ) OMEGA = OMEGA + 360.0d0
            end if

            GETMODEL = GETMIN (V(20),V(19),ECC,OMEGA,TIME)


        else if ( DTYPE == 4 ) then
            ! simply output third light value
            GETMODEL = V(15)


        else if ( DTYPE == 5 .or. DTYPE == 6 ) then
            ! DTYPE=5 for e*cos(omega)
            ! DTYPE=6 for e*sin(omega)

            if ( V(7) > 5.0d0 ) then
                ECC = V(7)
                OMEGA = V(8)
                ECOSW = (V(7)-10.0d0) * cos(V(8)/57.2957795d0)
                ESINW = (V(7)-10.0d0) * sin(V(8)/57.2957795d0)
                if ( DTYPE == 5 )  GETMODEL = V(7)!ECC
                if ( DTYPE == 6 )  GETMODEL = V(8)!OMEGA
            else
                ECOSW = V(7)
                ESINW = V(8)
                ECC = sqrt(V(7)**2 + V(8)**2)
                OMEGA = atan2(V(8),V(7)) * 45.0d0 / atan(1.0d0)
                if ( OMEGA < 0.0d0 ) OMEGA = OMEGA + 360.0d0
                if ( OMEGA > 360.0d0 ) OMEGA = OMEGA - 360.0d0
                if ( DTYPE == 5 )  GETMODEL = ECOSW
                if ( DTYPE == 6 )  GETMODEL = ESINW
            end if


        else
            GETMODEL = -100.0d0
            write(6,*) "### ERROR: wrong datatype asked for in GETMODEL: ",DTYPE
            STOP
        end if


  END FUNCTION GETMODEL




!=======================================================================
      SUBROUTINE LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,HJD,FMAG,LP,LS)
	USE consts
      IMPLICIT NONE
!       DOUBLE PRECISION,INTENT(IN) :: V(68),
	DOUBLE PRECISION,INTENT(IN) :: HJD
!       DOUBLE PRECISION :: GETPHASE
      DOUBLE PRECISION,INTENT(OUT) :: LP,LS,FMAG
      DOUBLE PRECISION :: LECL,LE
      DOUBLE PRECISION :: LD1U,LD2U              ! linear LD coeff for each star
      DOUBLE PRECISION :: LD1Q,LD2Q              ! quadratic LD coeff for each star
      DOUBLE PRECISION :: LD1S,LD2S              ! square-root LD coeff for each star
      DOUBLE PRECISION :: LD1L,LD2L              ! logarithmic LD coeff for each star
      DOUBLE PRECISION :: LD1C,LD2C              ! cubic LD coeff for each star
      DOUBLE PRECISION :: LDU,LDQ,LDS,LDL,LDC    ! LD coeffs for the star in question
      INTEGER,INTENT(IN) :: LDTYPE(2)           ! LD law type for both stars
      INTEGER,PARAMETER :: GIMENEZ=3             ! 1 to use original FMAX calculations
                                  ! 2 to use Gimenez' modified calcs
                                  ! 3 to use Gimenez' nonlinear LD calcs
      INTEGER :: i,j
      INTEGER,INTENT(INOUT) :: NSINE,PSINE(5)
      INTEGER,INTENT(INOUT) :: NPOLY,PPOLY(5)
      DOUBLE PRECISION :: PHASE,SINT,SINP,SINA,SINTERM,LPMULT,LSMULT
      DOUBLE PRECISION :: BS,FI,YP,YS,SP,SS,Q,TANGL,EL,DPH,SFACT,DGAM
      DOUBLE PRECISION :: ECOSW,ESINW,E,W
      DOUBLE PRECISION :: FMAXP,FMAXS,DELTP, DELTS, SHORT
!       data GIMENEZ / 3 /
!       data PI,TWOPI,RAD / 3.1415926536E0,6.28318531E0,0.0174532925E0 /
!       data LPMULT,LSMULT / 1.0 , 1.0 /

      DOUBLE PRECISION :: RP,RS,FMN,GAM,GAMN,FMA,FMINP,A,A1,AA,AB1,AA1
      DOUBLE PRECISION :: B2,ALAST,ADISK,AREA,B1,DLP,DLE,COSGAM,COS2
      DOUBLE PRECISION :: COSE,DD,COSW,COSI2,COSV,COSVW,CSVWT,DAREA
      DOUBLE PRECISION :: DGM,DENOM,DGAMA,DISC,E0,DLS,EA,B,ALPHA,D,ES
      DOUBLE PRECISION :: FMA0,EP,FLITE,HEAT,FMINS,HEAT2,OS,OP,OMEGA,OTOT
      DOUBLE PRECISION :: RR,R1,R2,R12,R22,REFL,RPA,RPB,S1,S,RV,RSB
      DOUBLE PRECISION :: SINE,SIN2,SINI2,SINI,SINW,SINVW,SINV,SUM,TANGR
      DOUBLE PRECISION :: THETA,RAS,RK,RSA
      

!       GIMENEZ = 3
      LPMULT = 1.0
      LSMULT = 1.0
!      PI = 3.1415926536E0
!      TWOPI = 6.28318531E0
!      RAD = 0.0174532925E0



!       DETERMINE PRIMARY AND SECONDARY BIAXIAL DIMENSIONS
!       USE SPHERICAL RADII FOR THE COMPUTATION OF ECLIPSE FUNCTIONS
!       USE OBLATENESSES FOR THE COMPUTATION OF THE OUTSIDE ECLIPSE
!       PHOTOMETRI!VARIATIONS WITH LIMB AND GRAVITY DARKENING


      if ( V(3) >= 0.0d0 ) then
        RP   = real( V(2)/(1.0d0+V(3)) )
        RS   = real( V(2)/(1.0d0+(1.0d0/V(3))) )
      else
        RP   = real( V(2) )
        RS   = abs( real( V(3) ) )
      end if
!       write (35,*) v(2), v(3), rp, rs
! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !       RATIO  = real(V(3))

      if ( V(7) > 5.0d0 ) then
        ECOSW = real( (V(7)-10.0d0) * cos(V(8)/57.2957795d0) )
        ESINW = real( (V(7)-10.0d0) * sin(V(8)/57.2957795d0) )
      else
        ECOSW  = real(V( 7))
        ESINW  = real(V( 8))
      end if


      BS     = real( V(1) )
      FI     = real( V(6) )
      YP     = real( V(9) )
      YS     = real( V(10) )
      SP     = real( V(11) )
      SS     = real( V(12) )
      Q      = real( V(13) )
      TANGL  = real( V(14) )
      EL     = real( V(15) )
      DPH    = 1.0 - real( V(16) )
      SFACT  = real( V(17) )
      DGAM   = real( V(18) )

      LD1U = real(V(4))         ! linear terms
      LD2U = real(V(5))

      ! log terms
      LD1L = 0.0                
      LD2L = 0.0

      ! sqrt terms
      LD1S = 0.0                
      LD2S = 0.0

      ! quadratic terms
      LD1Q = 0.0                
      LD2Q = 0.0

      ! cubic terms
      LD1C= 0.0                
      LD2C= 0.0

      if ( LDTYPE(1)==2 ) LD1L = real(V(21))
      if ( LDTYPE(1)==3 ) LD1S = real(V(21))
      if ( LDTYPE(1)==4 ) LD1Q = real(V(21))
      if ( LDTYPE(1)==5 ) LD1C = real(V(21))
      if ( LDTYPE(2)==2 ) LD2L = real(V(22))
      if ( LDTYPE(2)==3 ) LD2S = real(V(22))
      if ( LDTYPE(2)==4 ) LD2Q = real(V(22))
      if ( LDTYPE(2)==5 ) LD2C= real(V(22))

      if ( LDTYPE(2)==0 ) then
        LD2U = LD1U
        LD2L = LD1L
        LD2S = LD1S
        LD2Q = LD1Q
        LD2C= LD1C
      end if

      if ( NSINE > 0 ) then
        do i = 1,NSINE
          SINT = V(20+i*3)      ! sine reference time
          SINP = V(21+i*3)      ! sine period
          SINA = V(22+i*3)      ! sine amplitude
!           TWOPI =  atan(1.0d0) * 8.0d0
          SINTERM = SINA * sin( TWOPI * (HJD-SINT) / SINP )
          if ( PSINE(i) ==  1 )  BS = BS * (1.0+SINTERM)
          if ( PSINE(i) ==  2 )  RP = RP * (1.0+SINTERM)
          if ( PSINE(i) ==  3 )  RS = RS * (1.0+SINTERM)
          if ( PSINE(i) ==  6 )  FI = FI + SINTERM
          if ( PSINE(i) == 15 )  EL = EL * (1.0+SINTERM)
          if ( PSINE(i) == 17 )  SFACT = SFACT + SINTERM
          if ( PSINE(i) == -1 )  LPMULT = LPMULT * (1.0+SINTERM)
          if ( PSINE(i) == -2 )  LSMULT = LSMULT * (1.0+SINTERM)
        end do
      end if

      if ( NPOLY > 0 ) then
        do i = 1,NPOLY
          j = 32 + (i*6)
          SINTERM = V(j+1)*(HJD-V(j)) + V(j+2)*((HJD-V(j))**2) + V(j+3)* &
       ((HJD-V(j))**3) + V(j+4)*((HJD-V(j))**4)+ V(j+5)*((HJD-V(j))**5)
          if ( PPOLY(i) ==  1 )  BS = BS + SINTERM   ! Yes this is POLY-
          if ( PPOLY(i) ==  2 )  RP = RP + SINTERM   ! TERM but's called
          if ( PPOLY(i) ==  3 )  RS = RS + SINTERM   ! SINETERM to avoid
          if ( PPOLY(i) ==  6 )  FI = FI + SINTERM    ! proliferation of
          if ( PPOLY(i) == 15 )  EL = EL + SINTERM           ! variables
          if ( PPOLY(i) == 17 )  SFACT = SFACT + SINTERM
          if ( PPOLY(i) == -1 )  LPMULT = LPMULT + SINTERM
          if ( PPOLY(i) == -2 )  LSMULT = LSMULT + SINTERM
        end do
      end if

      PHASE = real(GETPHASE(HJD,V(19),V(20)))

! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !         RS=RP*RATIO
      if ( Q <= 0.0 ) then
        CALL BIAX (RP,0.d0,RPA,RPB,EP)
        CALL BIAX (RS,0.d0,RSA,RSB,ES)
      else
        CALL BIAX (RP,Q,RPA,RPB,EP)
        CALL BIAX (RS,1.d0/Q,RSA,RSB,ES)
      end if


!       CORRECT THE OBSERVED PHASE FOR ANY EPOCH ERROR IN EPHEMERIS

      THETA=PHASE+DPH

      SINI  = SIN(FI*RAD)
      SINI2 = SINI*SINI
      COSI2 = 1.0E0  - SINI2

!       TRANSLATE TIDAL LEAD/LAG ANGLE TO RADIANS
      TANGR=TANGL*RAD

!    EQUATION 9
!       CONVERT PHASE TO RADIANS
      FMN=THETA*TWOPI

!       GET CURRENT VALUES OF E, AND W
      CALL GETEW (ECOSW,ESINW,E,W)
! 		write(*,*) ECOSW,ESINW,E,W

!       TEST FOR CIRCULAR ORBIT
      IF (E)   17,20,17
   20 COSVW=COS(FMN)
      SINVW=SIN(FMN)
      RV=1.0E0
      GO TO 25

!       SOLUTION OF KEPLER'S EQUATION BY DIFFERENTIAL CORRECTIONS
!       (NON-ZERO ECCENTRICITY ONLY . . . )

!    EQUATION 6

   17 OMEGA = 450.0E0  - W
   23 IF (OMEGA - 360.0E0)         22,21,21
   21 OMEGA = OMEGA - 360.0E0
      GO TO 23
   22 OMEGA = OMEGA*RAD
!       SINE AND COSINE OF OMEGA
      COSW=COS(OMEGA)
      SINW=SIN(OMEGA)

!       COMPUTE MEAN ANOMALY CORRECTION TO PHASE
!       CORRESPONDING TO V=OMEGA=90-W
!       AT WHICH PHASE COS(V-OMEGA)=1
      E0=ATAN2(SQRT(1.0E0-E*E)*SINW,COSW+E)

!       MEAN ANOMALY OF MID-PRIMARY ECLIPSE
      FMA0=E0-E*SIN(E0)

!       MEAN ANOMALY
      FMA=FMN+FMA0
!    FIRST APPROXIMATION OF ECCENTRI!ANOMALY
      EA=FMA+E*SIN(FMA)

      DO 10 J=1,15
!       EVALUATE SINE AND COSINE OF ECCENTRI!ANOMALY
      SINE=SIN(EA)
      COSE=COS(EA)
      DENOM=1.0E0-E*COSE
      DISC=FMA-EA+E*SINE
      EA=EA+DISC/DENOM
!       TEST FOR CONVERGENCE
      IF (ABS(DISC) - 2.0E-05)     15,15,10
   10 CONTINUE


!       EVALUATE SINE AND COSINE OF TRUE ANOMALY
   15 COSV=(COSE-E)/DENOM
      SINV=SINE*SQRT(1.0E0-E*E)/DENOM

!       RADIUS VECTOR
      RV = (1.0E0-E*E)/(1.0E0+E*COSV)

!       THE PHOTOMETRI!PHASE ARGUMENT IN TERMS OF ORBIT PARAMETERS
!       VW = V-OMEGA
      COSVW=COSV*COSW+SINV*SINW
      SINVW=SINV*COSW-COSV*SINW

   25 COS2=COSVW*COSVW
      SIN2=1.0E0-COS2

      CSVWT=COS(TANGR)*COSVW-SIN(TANGR)*SINVW


!       PHOTOMETRI!EFFECTS



      FMAXP = 0.0
      FMAXS = 0.0
      DELTP = 0.0
      DELTS = 0.0
      SHORT = 0.0

!-----------------------------------------------------------------------
! Alvaro Gimenez and J Diaz-Cordoves have corrected the treatment of LD
! and stellar shapes.  This treatment can be used by putting GIMENEZ=2
! Their teatment for nonlinear LD can be used by putting GIMENEZ=3
!-----------------------------------------------------------------------
! This whole thing affects only the brightness normalisation of the two
! eclipsing stars: any problems here affect the radiative parameters
! but not the geometric parameters (radii, inclination etc).
!-----------------------------------------------------------------------

      if ( GIMENEZ==1 ) then                          ! LINEAR LD ONLY

!       FMAXP=((1.0E0-UP)+0.666666667E0*UP*(1.0E0+0.2E0*EP)) ! Original
!      1      *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)                ! lines
!       FMAXS=((1.0E0-US)+0.666666667E0*US*(1.0E0+0.2E0*ES)) ! if the
!      1      *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)                ! stars
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP    ! are
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES    ! oblate
!       SHORT=SINI2*CSVWT*CSVWT

!    26 FMAXP=1.0E0-UP/3.0E0                                 ! Original
!       FMAXS=1.0E0-US/3.0E0                                 ! lines if
!       DELTP=0.0E0                                          ! the stars
!       DELTS=0.0E0                                          ! are
!       SHORT=0.0                                            ! spherical

        if ( Q >= 0.0 ) then
          FMAXP=((1.0E0-LD1U)+0.666666667E0*LD1U*(1.0E0+0.2E0*EP)) &
             *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)
          FMAXS=((1.0E0-LD2U)+0.666666667E0*LD2U*(1.0E0+0.2E0*ES)) &
             *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)
          DELTP=(15.0E0+LD1U)/(15.0E0-5.0E0*LD1U)*(1.0E0+YP)*EP
          DELTS=(15.0E0+LD2U)/(15.0E0-5.0E0*LD2U)*(1.0E0+YS)*ES
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0E0-LD1U/3.0E0
          FMAXS=1.0E0-LD2U/3.0E0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!-----------------------------------------------------------------------

      else if ( GIMENEZ==2 ) then                     ! LINEAR LD ONLY

!       FMAXP=(1.0E0-UP*(1.0E0-2.0E0/5.0E0*EP)/3.0E0+YP*EP   ! Original
!      1      *(3.0E0-13.0E0/15.0E0*UP))/(1.0E0-EP)          ! lines
!       FMAXS=(1.0E0-US*(1.0E0-2.0E0/5.0E0*ES)/3.0E0+YS*ES   ! if the
!      1      *(3.0E0-13.0E0/15.0E0*US))/(1.0E0-ES)          ! stars
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP    ! are
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES    ! oblate
!       SHORT=SINI2*CSVWT*CSVWT

!    26 FMAXP=1.0E0-UP/3.0E0                                 ! Original
!       FMAXS=1.0E0-US/3.0E0                                 ! lines if
!       DELTP=0.0E0                                          ! the stars
!       DELTS=0.0E0                                          ! are
!       SHORT=0.0                                            ! spherical

        if ( Q >= 0.0 ) then
          FMAXP=(1.0E0-LD1U*(1.0E0-2.0E0/5.0E0*EP)/3.0E0+YP*EP &
               *(3.0E0-13.0E0/15.0E0*LD1U))/(1.0E0-EP)
         FMAXS=(1.0E0-LD2U*(1.0E0-2.0E0/5.0E0*ES)/3.0E0+YS*ES &
               *(3.0E0-13.0E0/15.0E0*LD2U))/(1.0E0-ES)
          DELTP=(15.0E0+LD1U)/(15.0E0-5.0E0*LD1U)*(1.0E0+YP)*EP
          DELTS=(15.0E0+LD2U)/(15.0E0-5.0E0*LD2U)*(1.0E0+YS)*ES
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0E0-LD1U/3.0E0
          FMAXS=1.0E0-LD2U/3.0E0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!-----------------------------------------------------------------------
! And this is Gimenez's code for including nonlinear LD. He includes
! the linear (UP), quadratic (UP, U2P) and square-root (UP, U3P) laws.
!-----------------------------------------------------------------------

      else if ( GIMENEZ==3 ) then

!      FMAXP=1.0E0-UP*(1.0E0-2.0E0*EP/5.0E0)/3.0E0-
!     1      U2P*(1.0E0-3.0E0*EP/5.0E0)/6.0E0-
!     1      U3P*(1.0E0-4.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP
!     1      *(1.5E0-13.0E0*UP/30.0E0-U2P/5.0E0-23.0E0*U3P/90.0E0)
!      FMAXP=FMAXP/(1.0E0-EP)
!      FMINP=1.0E0-UP*(1.0E0+4.0E0*EP/5.0E0)/3.0E0-
!     1      U2P*(1.0E0+6.0E0*EP/5.0E0)/6.0E0-
!     1      U3P*(1.0E0+8.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP
!     1      *(1.0E0-7.0E0*UP/15.0E0-4.0E0*U2P/15.0E0-13.0E0*U3P/45.0E0)
!      FMINS=1.0E0-US*(1.0E0+4.0E0*ES/5.0E0)/3.0E0-
!     1      U2S*(1.0E0+6.0E0*ES/5.0E0)/6.0E0-
!     1      U3S*(1.0E0+8.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES
!     1      *(1.0E0-7.0E0*US/15.0E0-4.0E0*U2S/15.0E0-13.0E0*U3S/45.0E0)
!      FMAXS=1.0E0-US*(1.0E0-2.0E0*ES/5.0E0)/3.0E0-
!     1      U2S*(1.0E0-3.0E0*ES/5.0E0)/6.0E0-
!     1      U3S*(1.0E0-4.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES
!     1      *(1.5E0-13.0E0*US/30.0E0-U2S/5.0E0-23.0E0*U3S/90.0E0)
!      FMAXS=FMAXS/(1.0E0-ES)
!      DELTP=1.0E0-FMINP/FMAXP
!      DELTS=1.0E0-FMINS/FMAXS
!      SHORT=SINI2*CSVWT*CSVWT

!   26 FMAXP=1.0E0-UP/3.0E0-U2P/6.0E0-U3P/5.0E0
!      FMAXS=1.0E0-US/3.0E0-U2S/6.0E0-U3S/5.0E0
!      DELTP=0.0E0
!      DELTS=0.0E0
!      SHORT=0.0

        if ( Q >= 0.0d0 .or. LDTYPE(1)==1 .or. LDTYPE(1)==5 .or. &
                             LDTYPE(2)==1 .or. LDTYPE(2)==5 ) then
          FMAXP=1.0E0-LD1U*(1.0E0-2.0E0*EP/5.0E0)/3.0E0- &
               LD1Q*(1.0E0-3.0E0*EP/5.0E0)/6.0E0- &
               LD1S*(1.0E0-4.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP &
              *(1.5E0-13.0E0*LD1U/30.0E0-LD1Q/5.0E0-23.0E0*LD1S/90.0E0)
          FMAXP=FMAXP/(1.0E0-EP)
          FMINP=1.0E0-LD1U*(1.0E0+4.0E0*EP/5.0E0)/3.0E0- &
               LD1Q*(1.0E0+6.0E0*EP/5.0E0)/6.0E0- &
               LD1S*(1.0E0+8.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP &
        *(1.0E0-7.0E0*LD1U/15.0E0-4.0E0*LD1Q/15.0E0-13.0E0*LD1S/45.0E0)
          FMINS=1.0E0-LD2U*(1.0E0+4.0E0*ES/5.0E0)/3.0E0- &
               LD2Q*(1.0E0+6.0E0*ES/5.0E0)/6.0E0- &
               LD2S*(1.0E0+8.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES &
        *(1.0E0-7.0E0*LD2U/15.0E0-4.0E0*LD2Q/15.0E0-13.0E0*LD2S/45.0E0)
          FMAXS=1.0E0-LD2U*(1.0E0-2.0E0*ES/5.0E0)/3.0E0- &
              LD2Q*(1.0E0-3.0E0*ES/5.0E0)/6.0E0- &
               LD2S*(1.0E0-4.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES &
              *(1.5E0-13.0E0*LD2U/30.0E0-LD2Q/5.0E0-23.0E0*LD2S/90.0E0)
          FMAXS=FMAXS/(1.0E0-ES)
          DELTP=1.0E0-FMINP/FMAXP
          DELTS=1.0E0-FMINS/FMAXS
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0-LD1U/3.0-LD1Q/6.0-LD1S/5.0+LD1L*2.0/9.0-LD1C/10.0
          FMAXS=1.0-LD2U/3.0-LD2Q/6.0-LD2S/5.0+LD2L*2.0/9.0-LD2C/10.0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!----------------------------------------------------------------------
      end if
!----------------------------------------------------------------------
! Complete original code before the above messing:
! C
! C
! !       PHOTOMETRI!EFFECTS
! C
! C
! !       TEST FOR SIMPLE CASE OF TWO SPHERICAL STARS
!       IF (EP .EQ. 0.  .AND.  ES .EQ. 0.)   GO TO 26
! C
! !       EITHER OR BOTH STARS ARE OBLATE
! C
!       FMAXP=((1.0E0-UP)+0.666666667E0*UP*(1.0E0+0.2E0*EP))
!      1      *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)
!       FMAXS=((1.0E0-US)+0.666666667E0*US*(1.0E0+0.2E0*ES))
!      1      *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)
! !       CHANGE IN INTENSITY RATIO DUE TO OBLATENESS RELATED VARIABLES
! !       FROM QUADRATURE TO MINIMUM
! !       FACE ON TO END ON
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES
! !       FORE-SHORTENING FUNCTION OF OBLATENESS
!       SHORT=SINI2*CSVWT*CSVWT
!       GO TO 27
! C
! !       BOTH STARS ARE SPHERICAL
! C
!    26 FMAXP=1.0E0-UP/3.0E0
!       FMAXS=1.0E0-US/3.0E0
!       DELTP=0.0E0
!       DELTS=0.0E0
!       SHORT=0.0
!----------------------------------------------------------------------


!       UN-NORMALIZED BRIGHTNESS OF STELLAR COMPONENTS AT QUADRATURE
   27 OP=PI*RPB*RPB*FMAXP
      OS=PI*RSB*RSB*FMAXS*BS
!       THE NORMALIZING FACTOR
      OTOT=OP+OS
!       BRIGHTNESS CONTRIBUTION FROM EACH COMPONENT
      LP=OP/OTOT*(1.0E0-DELTP*SHORT)
      LS=OS/OTOT*(1.0E0-DELTS*SHORT)

!       REFLECTION AND RERADIATION EQUATION
      IF (SP .EQ. 0.0E0  .AND.  SS .EQ. 0.0E0)   GO TO 28
      HEAT=SINI*COSVW
      HEAT2=0.5E0+0.5E0*HEAT*HEAT
      DLP=SP*(HEAT2+HEAT)
      DLS=SS*(HEAT2-HEAT)
      GO TO 29
   28 DLP=0.0E0
      DLS=0.0E0

!       WHICH ECLIPSE COULD THIS BE
   29 IF (COSVW)         40,40,30

!    PRIMARY ECLIPSE

   30 R1 = RP
      R2 = RS
!----------------------------------------------------------------------!
! JKT mod (10/8/2006): the line these replaced was      UU = UP        !
!----------------------------------------------------------------------!
      LDU = LD1U                                                       !
      LDL = LD1L                                                       !
      LDS = LD1S                                                       !
      LDQ = LD1Q                                                       !
      LDC= LD1C                                                      !
!----------------------------------------------------------------------!
      LE=LP
      DLE=DLP
      GO TO 60


!    SECONDARY ECLIPSE

   40 R1 = RS
      R2 = RP
!-----------------------------------------------------------------------
! JKT mod (10/8/2006): the line these replaced was      UU = US        !
!----------------------------------------------------------------------!
      LDU = LD2U                                                       !
      LDL = LD2L                                                       !
      LDS = LD2S                                                       !
      LDQ = LD2Q                                                       !
      LDC= LD2C                                                      !
!----------------------------------------------------------------------!
      LE=LS
      DLE=DLS

   60 SUM = 0.0E0
      ALAST = 0.0E0
      AREA=0.0E0

!    EQUATION  5

      DD = SINVW*SINVW + COSVW*COSVW*COSI2
      IF (DD .LE. 1.0E-06)  DD=0.0
      DD = DD*RV*RV
      D = SQRT(ABS(DD))
      R22 = R2*R2

!    EQUATION 17

      GAMN = 90.01E0*RAD
      DGAMA = DGAM*RAD
      DGM = DGAMA/2.0E0
      RK = 0.0E0
      GAM = 0.0E0
   50 GAM = GAM + DGAMA
!       HAS LIMIT OF INTEGRATION BEEN REACHED
      IF (GAM - GAMN)              48,48,49

   48 RR = R1*SIN(GAM)
      R12 = RR*RR

      AA = 0.0E0
!       ARE THE PROJECTED DISKS CONCENTRIC
      IF (D)                       405,406,405
  406 IF (RR - R2)                 230,230,403
  403 IF (RK - R2)                 404, 49, 49
  404 AA = PI*R22
      GO TO 215
!       TEST FOR NO ECLIPSE
  405 IF (D-R1-R2)                 240,216,216
  216 SUM = 0.0E0
      GO TO 49
!       DECIDE WHICH AREA EQUATIONS FOR NON-CONCENTRI!ECLIPSE
  240 IF (D-RR-R2)                 245,215,215
  245 IF (D-R2+RR)                 230,230,250
  250 IF (R1-R2)                   255,255,280
  255 IF (DD-R22+R12)              205,210,210
  280 IF (D-RR+R2)                 290,260,260
  260 IF (RR-R2)                   255,255,265
  265 IF (DD-R12+R22)              270,210,210

!    EQUATION 12

  270 S1 = ABS((R12 - R22 - DD)*0.5E0/D)
      A1 = ABS(R2-S1)
      B2 = ABS(RR-S1-D  )
      AA=PI*R22-(R22*ACOS((R2-A1)/R2) &
        - (R2-A1)*SQRT(2.0E0*R2*A1-A1*A1)) &
       +R12*ACOS((RR-B2)/RR)-(RR-B2)*SQRT(2.0E0*RR*B2-B2*B2)
      GO TO 215

  290 IF (R1 - R2 - D)             260,260,295
  295 IF (RK - R2 - D)             300,215,215
  300 RR = R2 + D
      R12 = RR*RR
      GAMN = 0.0E0
      GO TO 260

  230 AA = PI*R12
      GO TO 215

!    EQUATION 10

  205 S = ABS((R12 - R22 + DD)*0.5E0/D)
      A = ABS(RR-S)
      B1 = ABS(R2-S-D)
      A1 = R12*ACOS((RR-A)/RR) - (RR-A)*SQRT(2.0E0*RR*A - A*A)
      AB1 = R22*ACOS((R2-B1)/R2) - (R2-B1)*SQRT(2.0E0*R2*B1-B1*B1)
      AA = PI*R12 - A1 + AB1
      GO TO 215

!    EQUATION 1

  210 S = ABS((R12 - R22 + DD)*0.5E0/D)
      A = ABS(RR-S)
      B = ABS(S-D+R2)
      A1 = R12*ACOS((RR-A)/RR) - (RR-A)*SQRT(2.0E0*RR*A - A*A)
      AA1 = R22*ACOS((R2-B)/R2) - (R2-B)*SQRT(2.0E0*R2*B - B*B)
      AA = A1 + AA1

  215 DAREA = AA - ALAST
!----------------------------------------------------------------------!
! JKT modification (10/9/2006). The removed line was:                  !
!     SUM = SUM + DAREA*(1.0E0  - UU + UU*COS(GAM-DGM))                !
!----------------------------------------------------------------------!
      COSGAM = cos(GAM-DGM)                                            !
      SUM = SUM + DAREA*(1.0 - LDU*(1.0-COSGAM) &                        !
              - LDL*COSGAM*log(COSGAM) - LDS*(1.0-sqrt(COSGAM))  &     !
             - LDQ*(1.0-COSGAM)**2 - LDC*(1.0-COSGAM)**3)            !
!----------------------------------------------------------------------!
      ALAST = AA
      AREA = AREA + DAREA

      RK = RR
      GO TO 50

!       LIGHT LOSS FROM ECLIPSE

   49 ADISK = PI*R1*R1
!----------------------------------------------------------------------!
! JKT modification (10/9/2006).  See 1992A+A...259..227D for more info.!
! The removed line was:           ALPHA = SUM/(ADISK*(1.0E0-UU/3.0E0)) !
!----------------------------------------------------------------------!
      ALPHA = 1.0 - LDU/3.0 + LDL*2.0/9.0 - LDS/5.0 - LDQ/6.0 - LDC/10.0
      ALPHA = SUM/(ADISK*ALPHA)                                        !
!----------------------------------------------------------------------!
      LECL = ALPHA*LE
      AREA = AREA/ADISK
      REFL=DLP+DLS-AREA*DLE

!       THEORETICAL INTENSITY WITH THIRD LIGHT AND QUADRATURE
!       SCALE FACTOR APPLIED

!----------------------------------------------------------------------!
! This is the original line from EBOP:
!----------------------------------------------------------------------!
!      FLITE = ((LP+LS-LECL+REFL)*(1.0E0-EL)+EL)*SFACT
!----------------------------------------------------------------------!

      LP = LP * LPMULT               ! sine/poly applied to star 1 light
      LS = LS * LSMULT               ! sine/poly applied to star 2 light
      FLITE = ((LP+LS-LECL+REFL)*(1.0E0-EL)+EL)
      FMAG = -2.5d0 * log10(FLITE) + SFACT

      LP = LP * (1.0E0-EL)           ! account for third light *AFTER*
      LS = LS * (1.0E0-EL)           ! FLITE and FMAG have been found

      END SUBROUTINE light




  SUBROUTINE BIAX (R,Q,A,B,EPS)
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: R,Q
      DOUBLE PRECISION, INTENT(OUT) :: A,B,EPS
      DOUBLE PRECISION :: q13,fq

      ! EBOP subroutine to calculate biaxial ellipsoid dimensions
      ! and oblateness for each star after Chandrasekhar (1933).

      if ( Q <= 0.0 )  then
        A = R
        B = R
        EPS = 0.0
      else
        A = R * ( 1.0 + (1.0 + 7.0*Q)/6.0 * R**3.0)
        B = R * ( 1.0 + (1.0 - 2.0*Q)/6.0 * R**3.0)
        !  		write(*,*) "b1",R,Q,A,B,EPS
        EPS = (A - B) / A
        B=( (1.0 - EPS) * R**3.0) ** (1.0/3.0)
        A = B / (1.0 - EPS)
        !  		write(*,*) "b2",R,Q,A,B,EPS
      end if

      ! 	  q13 = (Q)**(1.d0/3.d0)
      !       fq = 0.49d0*q13*q13/(0.6d0*q13*q13+LOG(1.d0+q13))
      !       B= fq
      ! 	  A=0.0
      ! 	  EPS=0.0
  END SUBROUTINE BIAX



  SUBROUTINE GETEW (ECOSW,ESINW,E,W)
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: ECOSW,ESINW
      DOUBLE PRECISION, INTENT(OUT) :: E,W
            ! EBOP subroutine to calculate e and w from e(cos)w e(sin)w

      if ( ECOSW==0.0  .and.  ESINW==0.0 ) then
        E = 0.0
        W = 0.0
      else
        W = atan2( ESINW,ECOSW )
        E = sqrt( ESINW*ESINW + ECOSW*ECOSW )
        W = W * 180.0 / 3.1415926536
      end if

  END SUBROUTINE GETEW




  DOUBLE PRECISION FUNCTION GETPHASE (HJD,PERIOD,TZERO)
            ! Returns phase from given time and orbital ephemeris
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: HJD,PERIOD,TZERO

       GETPHASE = (HJD - TZERO) / PERIOD
       GETPHASE = GETPHASE - int(GETPHASE)
       if ( GETPHASE < 0.0d0 ) GETPHASE = GETPHASE + 1.0d0

  END FUNCTION GETPHASE




  DOUBLE PRECISION FUNCTION GETMIN (TZERO,PERIOD,ECC,OMEGA,CYCLE)
      USE consts

      ! Returns time of minimum for given cycle and ephemeris.  If
      ! the orbit is circular thenthe cycle number is used without
      ! restriction so can refer to any phase. If the orbit is ec-
      ! centric then the cycle number should be integer (indicates
      ! primary minimum) or half-integer (secondary minimum).

      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: TZERO,PERIOD           ! IN: reference time, orbital period
      DOUBLE PRECISION,INTENT(IN) :: ECC,OMEGA              ! IN: orbital (e,w) or (ecosw,esinw)
      DOUBLE PRECISION,INTENT(IN) :: CYCLE                  ! IN: cycle number of minimum to use
      DOUBLE PRECISION :: E,W                    ! LOCAL: eccentricity and peri.long.
      DOUBLE PRECISION :: THETA,EE,TANEE         ! LOCAL: true and eccentric anomaly
      DOUBLE PRECISION :: PSEP                   ! LOCAL: phase diff between minima
      DOUBLE PRECISION :: TINY        ! LOCAL: useful variables

      TINY = 1.0d-6

      ! First must deal with the possibility that e and omega are
      ! actually e*cos(omega) and e*sin(omega)

      if ( ECC > 9.0d0 ) then
        E = ECC - 10.0d0
        W = OMEGA
      else
        E = sqrt( OMEGA*OMEGA + ECC*ECC )
        W = atan2( ECC,OMEGA )
      end if

      ! If orbit is circular then simply use the orbital ephemeris
      ! If orbit is eccentric then must calculate the phase diffe-
      ! rence between two successive primary and secondary minima.

      if ( abs(E) < TINY ) then
          GETMIN = TZERO  +  PERIOD * CYCLE

      else
          THETA = 3.0d0*PI/2.0d0 - W
          TANEE = sqrt( (1.0d0-E) / (1.0d0+E) ) * tan(THETA/2.0d0)
          EE = atan(TANEE) * 2.0d0
          PSEP = (EE - E*sin(EE)) / (2.0d0 * PI)
          if ( PSEP < 0.0d0 ) PSEP = PSEP + 1.0d0

          if ( mod(abs(CYCLE),1.0d0) < TINY ) then       
              ! primary minimum
              GETMIN = TZERO + PERIOD * CYCLE
          else                                         
              ! secondary minimum
              GETMIN = TZERO + PERIOD * int(CYCLE)
              if ( CYCLE < 1.0d0 ) GETMIN = GETMIN - PERIOD
              GETMIN = GETMIN + PERIOD*PSEP
          end if

      end if

  END FUNCTION GETMIN




  ELEMENTAL DOUBLE PRECISION FUNCTION Porb2a(Porb,m1,m2)
     
 
      !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ! Function to evaluate the semi-major axis from Kepler's 3rd law
      ! Bart Willems, 30/11/00
      !
      ! Porb is expressed in years
      ! m1 and m2 are expressed in solar masses
      ! a is expressed in solar radii
      !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: Porb,m1,m2
      DOUBLE PRECISION :: n
      DOUBLE PRECISION, PARAMETER :: pi = 3.141592654d0
      DOUBLE PRECISION, PARAMETER :: gn = 6.67259d-11*5.86676d18

      n = 2.d0*pi/Porb
      Porb2a = (gn*(m1+m2)/n**2)**(1.d0/3.d0)

      RETURN
      
  END FUNCTION Porb2a



END MODULE jktebop

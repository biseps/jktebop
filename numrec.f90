      MODULE nrtype
      INTEGER, PARAMETER :: I4B = SELECTED_INT_KIND(9)
      INTEGER, PARAMETER :: I2B = SELECTED_INT_KIND(4)
      INTEGER, PARAMETER :: I1B = SELECTED_INT_KIND(2)
      INTEGER, PARAMETER :: SP = KIND(1.0)
      INTEGER, PARAMETER :: DP = KIND(1.0D0)
      INTEGER, PARAMETER :: SPC = KIND((1.0,1.0))
      INTEGER, PARAMETER :: DPC = KIND((1.0D0,1.0D0))
      INTEGER, PARAMETER :: LGT = KIND(.true.)
      REAL(SP), PARAMETER :: PI=3.14159265358979323846264338327950288419
      REAL(SP), PARAMETER :: PIO2=1.570796326794896619231321691639751442
      REAL(SP), PARAMETER :: TWOPI=6.28318530717958647692528676655900576
      REAL(SP), PARAMETER :: SQRT2=1.41421356237309504880168872420969807
      REAL(SP), PARAMETER :: EULER=0.57721566490153286060651209008240243
      REAL(DP), PARAMETER :: PI_D=3.141592653589793238462643383279502884
      REAL(DP), PARAMETER :: PIO2_D=1.5707963267948966192313216916397514
      REAL(DP), PARAMETER :: TWOPI_D=6.283185307179586476925286766559005
      TYPE sprs2_sp
            INTEGER(I4B) :: n,len
            REAL(SP), DIMENSION(:), POINTER :: val
            INTEGER(I4B), DIMENSION(:), POINTER :: irow
            INTEGER(I4B), DIMENSION(:), POINTER :: jcol
      END TYPE sprs2_sp
      TYPE sprs2_dp
            INTEGER(I4B) :: n,len
            REAL(DP), DIMENSION(:), POINTER :: val
            INTEGER(I4B), DIMENSION(:), POINTER :: irow
            INTEGER(I4B), DIMENSION(:), POINTER :: jcol
      END TYPE sprs2_dp
      END MODULE nrtype

      MODULE nrutil
      USE nrtype
      IMPLICIT NONE
      INTEGER(I4B), PARAMETER :: NPAR_ARTH=16,NPAR2_ARTH=8
      INTEGER(I4B), PARAMETER :: NPAR_GEOP=4,NPAR2_GEOP=2
      INTEGER(I4B), PARAMETER :: NPAR_CUMSUM=16
      INTEGER(I4B), PARAMETER :: NPAR_CUMPROD=8
      INTEGER(I4B), PARAMETER :: NPAR_POLY=8
      INTEGER(I4B), PARAMETER :: NPAR_POLYTERM=8
      INTERFACE array_copy
            MODULE PROCEDURE array_copy_r, array_copy_d, array_copy_i
      END INTERFACE
      INTERFACE swap
            MODULE PROCEDURE swap_i,swap_r,swap_rv,swap_c, &
                        swap_cv,swap_cm,swap_z,swap_zv,swap_zm, &
                        masked_swap_rs,masked_swap_rv,masked_swap_rm
      END INTERFACE
      INTERFACE reallocate
            MODULE PROCEDURE reallocate_rv,reallocate_rm, &
                        reallocate_iv,reallocate_im,reallocate_hv
      END INTERFACE
      INTERFACE imaxloc
            MODULE PROCEDURE imaxloc_r,imaxloc_i
      END INTERFACE
      INTERFACE assert
            MODULE PROCEDURE assert1,assert2,assert3,assert4,assert_v
      END INTERFACE
      INTERFACE assert_eq
            MODULE PROCEDURE assert_eq2,assert_eq3,assert_eq4,assert_eqn
      END INTERFACE
      INTERFACE arth
            MODULE PROCEDURE arth_r, arth_d, arth_i
      END INTERFACE
      INTERFACE geop
            MODULE PROCEDURE geop_r, geop_d, geop_i, geop_c, geop_dv
      END INTERFACE
      INTERFACE cumsum
            MODULE PROCEDURE cumsum_r,cumsum_i
      END INTERFACE
      INTERFACE poly
            MODULE PROCEDURE poly_rr,poly_rrv,poly_dd,poly_ddv, &
                        poly_rc,poly_cc,poly_msk_rrv,poly_msk_ddv
      END INTERFACE
      INTERFACE poly_term
            MODULE PROCEDURE poly_term_rr,poly_term_cc
      END INTERFACE
      INTERFACE outerprod
            MODULE PROCEDURE outerprod_r,outerprod_d
      END INTERFACE
      INTERFACE outerdiff
            MODULE PROCEDURE outerdiff_r,outerdiff_d,outerdiff_i
      END INTERFACE
      INTERFACE scatter_add
            MODULE PROCEDURE scatter_add_r,scatter_add_d
      END INTERFACE
      INTERFACE scatter_max
            MODULE PROCEDURE scatter_max_r,scatter_max_d
      END INTERFACE
      INTERFACE diagadd
            MODULE PROCEDURE diagadd_rv,diagadd_r
      END INTERFACE
      INTERFACE diagmult
            MODULE PROCEDURE diagmult_rv,diagmult_r
      END INTERFACE
      INTERFACE get_diag
            MODULE PROCEDURE get_diag_rv, get_diag_dv
      END INTERFACE
      INTERFACE put_diag
            MODULE PROCEDURE put_diag_rv, put_diag_r
      END INTERFACE
      CONTAINS
!BL
      SUBROUTINE array_copy_r(src,dest,n_copied,n_not_copied)
      REAL(SP), DIMENSION(:), INTENT(IN) :: src
      REAL(SP), DIMENSION(:), INTENT(OUT) :: dest
      INTEGER(I4B), INTENT(OUT) :: n_copied, n_not_copied
      n_copied=min(size(src),size(dest))
      n_not_copied=size(src)-n_copied
      dest(1:n_copied)=src(1:n_copied)
      END SUBROUTINE array_copy_r
!BL
      SUBROUTINE array_copy_d(src,dest,n_copied,n_not_copied)
      REAL(DP), DIMENSION(:), INTENT(IN) :: src
      REAL(DP), DIMENSION(:), INTENT(OUT) :: dest
      INTEGER(I4B), INTENT(OUT) :: n_copied, n_not_copied
      n_copied=min(size(src),size(dest))
      n_not_copied=size(src)-n_copied
      dest(1:n_copied)=src(1:n_copied)
      END SUBROUTINE array_copy_d
!BL
      SUBROUTINE array_copy_i(src,dest,n_copied,n_not_copied)
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: src
      INTEGER(I4B), DIMENSION(:), INTENT(OUT) :: dest
      INTEGER(I4B), INTENT(OUT) :: n_copied, n_not_copied
      n_copied=min(size(src),size(dest))
      n_not_copied=size(src)-n_copied
      dest(1:n_copied)=src(1:n_copied)
      END SUBROUTINE array_copy_i
!BL
!BL
      SUBROUTINE swap_i(a,b)
      INTEGER(I4B), INTENT(INOUT) :: a,b
      INTEGER(I4B) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_i
!BL
      SUBROUTINE swap_r(a,b)
      REAL(SP), INTENT(INOUT) :: a,b
      REAL(SP) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_r
!BL
      SUBROUTINE swap_rv(a,b)
      REAL(SP), DIMENSION(:), INTENT(INOUT) :: a,b
      REAL(SP), DIMENSION(SIZE(a)) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_rv
!BL
      SUBROUTINE swap_c(a,b)
      COMPLEX(SPC), INTENT(INOUT) :: a,b
      COMPLEX(SPC) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_c
!BL
      SUBROUTINE swap_cv(a,b)
      COMPLEX(SPC), DIMENSION(:), INTENT(INOUT) :: a,b
      COMPLEX(SPC), DIMENSION(SIZE(a)) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_cv
!BL
      SUBROUTINE swap_cm(a,b)
      COMPLEX(SPC), DIMENSION(:,:), INTENT(INOUT) :: a,b
      COMPLEX(SPC), DIMENSION(size(a,1),size(a,2)) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_cm
!BL
      SUBROUTINE swap_z(a,b)
      COMPLEX(DPC), INTENT(INOUT) :: a,b
      COMPLEX(DPC) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_z
!BL
      SUBROUTINE swap_zv(a,b)
      COMPLEX(DPC), DIMENSION(:), INTENT(INOUT) :: a,b
      COMPLEX(DPC), DIMENSION(SIZE(a)) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_zv
!BL
      SUBROUTINE swap_zm(a,b)
      COMPLEX(DPC), DIMENSION(:,:), INTENT(INOUT) :: a,b
      COMPLEX(DPC), DIMENSION(size(a,1),size(a,2)) :: dum
      dum=a
      a=b
      b=dum
      END SUBROUTINE swap_zm
!BL
      SUBROUTINE masked_swap_rs(a,b,mask)
      REAL(SP), INTENT(INOUT) :: a,b
      LOGICAL(LGT), INTENT(IN) :: mask
      REAL(SP) :: swp
      if (mask) then
            swp=a
            a=b
            b=swp
      end if
      END SUBROUTINE masked_swap_rs
!BL
      SUBROUTINE masked_swap_rv(a,b,mask)
      REAL(SP), DIMENSION(:), INTENT(INOUT) :: a,b
      LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
      REAL(SP), DIMENSION(size(a)) :: swp
      where (mask)
            swp=a
            a=b
            b=swp
      end where
      END SUBROUTINE masked_swap_rv
!BL
      SUBROUTINE masked_swap_rm(a,b,mask)
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: a,b
      LOGICAL(LGT), DIMENSION(:,:), INTENT(IN) :: mask
      REAL(SP), DIMENSION(size(a,1),size(a,2)) :: swp
      where (mask)
            swp=a
            a=b
            b=swp
      end where
      END SUBROUTINE masked_swap_rm
!BL
!BL
      FUNCTION reallocate_rv(p,n)
      REAL(SP), DIMENSION(:), POINTER :: p, reallocate_rv
      INTEGER(I4B), INTENT(IN) :: n
      INTEGER(I4B) :: nold,ierr
      allocate(reallocate_rv(n),stat=ierr)
      if (ierr /= 0) call &
                  nrerror('reallocate_rv: problem in attempt to allocate')
      if (.not. associated(p)) RETURN
      nold=size(p)
      reallocate_rv(1:min(nold,n))=p(1:min(nold,n))
      deallocate(p)
      END FUNCTION reallocate_rv
!BL
      FUNCTION reallocate_iv(p,n)
      INTEGER(I4B), DIMENSION(:), POINTER :: p, reallocate_iv
      INTEGER(I4B), INTENT(IN) :: n
      INTEGER(I4B) :: nold,ierr
      allocate(reallocate_iv(n),stat=ierr)
      if (ierr /= 0) call &
                  nrerror('reallocate_iv: problem in attempt to allocate')
      if (.not. associated(p)) RETURN
      nold=size(p)
      reallocate_iv(1:min(nold,n))=p(1:min(nold,n))
      deallocate(p)
      END FUNCTION reallocate_iv
!BL
      FUNCTION reallocate_hv(p,n)
      CHARACTER(1), DIMENSION(:), POINTER :: p, reallocate_hv
      INTEGER(I4B), INTENT(IN) :: n
      INTEGER(I4B) :: nold,ierr
      allocate(reallocate_hv(n),stat=ierr)
      if (ierr /= 0) call &
                  nrerror('reallocate_hv: problem in attempt to allocate')
      if (.not. associated(p)) RETURN
      nold=size(p)
      reallocate_hv(1:min(nold,n))=p(1:min(nold,n))
      deallocate(p)
      END FUNCTION reallocate_hv
!BL
      FUNCTION reallocate_rm(p,n,m)
      REAL(SP), DIMENSION(:,:), POINTER :: p, reallocate_rm
      INTEGER(I4B), INTENT(IN) :: n,m
      INTEGER(I4B) :: nold,mold,ierr
      allocate(reallocate_rm(n,m),stat=ierr)
      if (ierr /= 0) call &
                  nrerror('reallocate_rm: problem in attempt to allocate')
      if (.not. associated(p)) RETURN
      nold=size(p,1)
      mold=size(p,2)
      reallocate_rm(1:min(nold,n),1:min(mold,m))= &
                  p(1:min(nold,n),1:min(mold,m))
      deallocate(p)
      END FUNCTION reallocate_rm
!BL
      FUNCTION reallocate_im(p,n,m)
      INTEGER(I4B), DIMENSION(:,:), POINTER :: p, reallocate_im
      INTEGER(I4B), INTENT(IN) :: n,m
      INTEGER(I4B) :: nold,mold,ierr
      allocate(reallocate_im(n,m),stat=ierr)
      if (ierr /= 0) call &
                  nrerror('reallocate_im: problem in attempt to allocate')
      if (.not. associated(p)) RETURN
      nold=size(p,1)
      mold=size(p,2)
      reallocate_im(1:min(nold,n),1:min(mold,m))= &
                  p(1:min(nold,n),1:min(mold,m))
      deallocate(p)
      END FUNCTION reallocate_im
!BL
      FUNCTION ifirstloc(mask)
      LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
      INTEGER(I4B) :: ifirstloc
      INTEGER(I4B), DIMENSION(1) :: loc
      loc=maxloc(merge(1,0,mask))
      ifirstloc=loc(1)
      if (.not. mask(ifirstloc)) ifirstloc=size(mask)+1
      END FUNCTION ifirstloc
!BL
      FUNCTION imaxloc_r(arr)
      REAL(SP), DIMENSION(:), INTENT(IN) :: arr
      INTEGER(I4B) :: imaxloc_r
      INTEGER(I4B), DIMENSION(1) :: imax
      imax=maxloc(arr(:))
      imaxloc_r=imax(1)
      END FUNCTION imaxloc_r
!BL
      FUNCTION imaxloc_i(iarr)
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: iarr
      INTEGER(I4B), DIMENSION(1) :: imax
      INTEGER(I4B) :: imaxloc_i
      imax=maxloc(iarr(:))
      imaxloc_i=imax(1)
      END FUNCTION imaxloc_i
!BL
      FUNCTION iminloc(arr)
      REAL(DP), DIMENSION(:), INTENT(IN) :: arr
      INTEGER(I4B), DIMENSION(1) :: imin
      INTEGER(I4B) :: iminloc
      imin=minloc(arr(:))
      iminloc=imin(1)
      END FUNCTION iminloc
!BL
      SUBROUTINE assert1(n1,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      LOGICAL, INTENT(IN) :: n1
      if (.not. n1) then
      		WRITE(0,*) 'ERROR numrec.f90 assert1 344'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
            STOP 
      end if
      END SUBROUTINE assert1
!BL
      SUBROUTINE assert2(n1,n2,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      LOGICAL, INTENT(IN) :: n1,n2
      if (.not. (n1 .and. n2)) then
      		WRITE(0,*) 'ERROR numrec.f90 assert2 356'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END SUBROUTINE assert2
!BL
      SUBROUTINE assert3(n1,n2,n3,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      LOGICAL, INTENT(IN) :: n1,n2,n3
      if (.not. (n1 .and. n2 .and. n3)) then
      		WRITE(0,*) 'ERROR numrec.f90 assert3 367'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END SUBROUTINE assert3
!BL
      SUBROUTINE assert4(n1,n2,n3,n4,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      LOGICAL, INTENT(IN) :: n1,n2,n3,n4
      if (.not. (n1 .and. n2 .and. n3 .and. n4)) then
      		WRITE(0,*) 'ERROR numrec.f90 assert4 378'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END SUBROUTINE assert4
!BL
      SUBROUTINE assert_v(n,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      LOGICAL, DIMENSION(:), INTENT(IN) :: n
      if (.not. all(n)) then
      		WRITE(0,*) 'ERROR numrec.f90 assert_v 389'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END SUBROUTINE assert_v
!BL
      FUNCTION assert_eq2(n1,n2,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      INTEGER, INTENT(IN) :: n1,n2
      INTEGER :: assert_eq2
      if (n1 == n2) then
            assert_eq2=n1
      else
      		WRITE(0,*) 'ERROR numrec.f90 assert_eq2 403'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END FUNCTION assert_eq2
!BL
      FUNCTION assert_eq3(n1,n2,n3,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      INTEGER, INTENT(IN) :: n1,n2,n3
      INTEGER :: assert_eq3
      if (n1 == n2 .and. n2 == n3) then
            assert_eq3=n1
      else
      		WRITE(0,*) 'ERROR numrec.f90 assert_eq3 417'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END FUNCTION assert_eq3
!BL
      FUNCTION assert_eq4(n1,n2,n3,n4,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      INTEGER, INTENT(IN) :: n1,n2,n3,n4
      INTEGER :: assert_eq4
      if (n1 == n2 .and. n2 == n3 .and. n3 == n4) then
            assert_eq4=n1
      else
      		WRITE(0,*) 'ERROR numrec.f90 assert_eq4 431'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END FUNCTION assert_eq4
!BL
      FUNCTION assert_eqn(nn,string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      INTEGER, DIMENSION(:), INTENT(IN) :: nn
      INTEGER :: assert_eqn
      if (all(nn(2:) == nn(1))) then
            assert_eqn=nn(1)
      else
      		WRITE(0,*) 'ERROR numrec.f90 assert_eqn 403'
            write (0,*) 'nrerror: an assertion failed with this tag:', &
                        string
            WRITE(0,*) 'END ERROR'
      end if
      END FUNCTION assert_eqn
!BL
      SUBROUTINE nrerror(string)
      CHARACTER(LEN=*), INTENT(IN) :: string
      write(0,*) ' ERROR numrec.f90 nrerror 454'
      write(0,*) 'nrerror: ',string
      write(0,*) 'END ERROR'
      STOP 
      END SUBROUTINE nrerror
!BL
      FUNCTION arth_r(first,increment,n)
      REAL(SP), INTENT(IN) :: first,increment
      INTEGER(I4B), INTENT(IN) :: n
      REAL(SP), DIMENSION(n) :: arth_r
      INTEGER(I4B) :: k,k2
      REAL(SP) :: temp
      if (n > 0) arth_r(1)=first
      if (n <= NPAR_ARTH) then
            do k=2,n
                  arth_r(k)=arth_r(k-1)+increment
            end do
      else
            do k=2,NPAR2_ARTH
                  arth_r(k)=arth_r(k-1)+increment
            end do
            temp=increment*NPAR2_ARTH
            k=NPAR2_ARTH
            do
                  if (k >= n) exit
                  k2=k+k
                  arth_r(k+1:min(k2,n))=temp+arth_r(1:min(k,n-k))
                  temp=temp+temp
                  k=k2
            end do
      end if
      END FUNCTION arth_r
!BL
      FUNCTION arth_d(first,increment,n)
      REAL(DP), INTENT(IN) :: first,increment
      INTEGER(I4B), INTENT(IN) :: n
      REAL(DP), DIMENSION(n) :: arth_d
      INTEGER(I4B) :: k,k2
      REAL(DP) :: temp
      if (n > 0) arth_d(1)=first
      if (n <= NPAR_ARTH) then
            do k=2,n
                  arth_d(k)=arth_d(k-1)+increment
            end do
      else
            do k=2,NPAR2_ARTH
                  arth_d(k)=arth_d(k-1)+increment
            end do
            temp=increment*NPAR2_ARTH
            k=NPAR2_ARTH
            do
                  if (k >= n) exit
                  k2=k+k
                  arth_d(k+1:min(k2,n))=temp+arth_d(1:min(k,n-k))
                  temp=temp+temp
                  k=k2
            end do
      end if
      END FUNCTION arth_d
!BL
      FUNCTION arth_i(first,increment,n)
      INTEGER(I4B), INTENT(IN) :: first,increment,n
      INTEGER(I4B), DIMENSION(n) :: arth_i
      INTEGER(I4B) :: k,k2,temp
      if (n > 0) arth_i(1)=first
      if (n <= NPAR_ARTH) then
            do k=2,n
                  arth_i(k)=arth_i(k-1)+increment
            end do
      else
            do k=2,NPAR2_ARTH
                  arth_i(k)=arth_i(k-1)+increment
            end do
            temp=increment*NPAR2_ARTH
            k=NPAR2_ARTH
            do
                  if (k >= n) exit
                  k2=k+k
                  arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
                  temp=temp+temp
                  k=k2
            end do
      end if
      END FUNCTION arth_i
!BL
!BL
      FUNCTION geop_r(first,factor,n)
      REAL(SP), INTENT(IN) :: first,factor
      INTEGER(I4B), INTENT(IN) :: n
      REAL(SP), DIMENSION(n) :: geop_r
      INTEGER(I4B) :: k,k2
      REAL(SP) :: temp
      if (n > 0) geop_r(1)=first
      if (n <= NPAR_GEOP) then
            do k=2,n
                  geop_r(k)=geop_r(k-1)*factor
            end do
      else
            do k=2,NPAR2_GEOP
                  geop_r(k)=geop_r(k-1)*factor
            end do
            temp=factor**NPAR2_GEOP
            k=NPAR2_GEOP
            do
                  if (k >= n) exit
                  k2=k+k
                  geop_r(k+1:min(k2,n))=temp*geop_r(1:min(k,n-k))
                  temp=temp*temp
                  k=k2
            end do
      end if
      END FUNCTION geop_r
!BL
      FUNCTION geop_d(first,factor,n)
      REAL(DP), INTENT(IN) :: first,factor
      INTEGER(I4B), INTENT(IN) :: n
      REAL(DP), DIMENSION(n) :: geop_d
      INTEGER(I4B) :: k,k2
      REAL(DP) :: temp
      if (n > 0) geop_d(1)=first
      if (n <= NPAR_GEOP) then
            do k=2,n
                  geop_d(k)=geop_d(k-1)*factor
            end do
      else
            do k=2,NPAR2_GEOP
                  geop_d(k)=geop_d(k-1)*factor
            end do
            temp=factor**NPAR2_GEOP
            k=NPAR2_GEOP
            do
                  if (k >= n) exit
                  k2=k+k
                  geop_d(k+1:min(k2,n))=temp*geop_d(1:min(k,n-k))
                  temp=temp*temp
                  k=k2
            end do
      end if
      END FUNCTION geop_d
!BL
      FUNCTION geop_i(first,factor,n)
      INTEGER(I4B), INTENT(IN) :: first,factor,n
      INTEGER(I4B), DIMENSION(n) :: geop_i
      INTEGER(I4B) :: k,k2,temp
      if (n > 0) geop_i(1)=first
      if (n <= NPAR_GEOP) then
            do k=2,n
                  geop_i(k)=geop_i(k-1)*factor
            end do
      else
            do k=2,NPAR2_GEOP
                  geop_i(k)=geop_i(k-1)*factor
            end do
            temp=factor**NPAR2_GEOP
            k=NPAR2_GEOP
            do
                  if (k >= n) exit
                  k2=k+k
                  geop_i(k+1:min(k2,n))=temp*geop_i(1:min(k,n-k))
                  temp=temp*temp
                  k=k2
            end do
      end if
      END FUNCTION geop_i
!BL
      FUNCTION geop_c(first,factor,n)
      COMPLEX(SP), INTENT(IN) :: first,factor
      INTEGER(I4B), INTENT(IN) :: n
      COMPLEX(SP), DIMENSION(n) :: geop_c
      INTEGER(I4B) :: k,k2
      COMPLEX(SP) :: temp
      if (n > 0) geop_c(1)=first
      if (n <= NPAR_GEOP) then
            do k=2,n
                  geop_c(k)=geop_c(k-1)*factor
            end do
      else
            do k=2,NPAR2_GEOP
                  geop_c(k)=geop_c(k-1)*factor
            end do
            temp=factor**NPAR2_GEOP
            k=NPAR2_GEOP
            do
                  if (k >= n) exit
                  k2=k+k
                  geop_c(k+1:min(k2,n))=temp*geop_c(1:min(k,n-k))
                  temp=temp*temp
                  k=k2
            end do
      end if
      END FUNCTION geop_c
!BL
      FUNCTION geop_dv(first,factor,n)
      REAL(DP), DIMENSION(:), INTENT(IN) :: first,factor
      INTEGER(I4B), INTENT(IN) :: n
      REAL(DP), DIMENSION(size(first),n) :: geop_dv
      INTEGER(I4B) :: k,k2
      REAL(DP), DIMENSION(size(first)) :: temp
      if (n > 0) geop_dv(:,1)=first(:)
      if (n <= NPAR_GEOP) then
            do k=2,n
                  geop_dv(:,k)=geop_dv(:,k-1)*factor(:)
            end do
      else
            do k=2,NPAR2_GEOP
                  geop_dv(:,k)=geop_dv(:,k-1)*factor(:)
            end do
            temp=factor**NPAR2_GEOP
            k=NPAR2_GEOP
            do
                  if (k >= n) exit
                  k2=k+k
                  geop_dv(:,k+1:min(k2,n))=geop_dv(:,1:min(k,n-k))* &
                              spread(temp,2,size(geop_dv(:,1:min(k,n-k))))
                  temp=temp*temp
                  k=k2
            end do
      end if
      END FUNCTION geop_dv
!BL
!BL
      RECURSIVE FUNCTION cumsum_r(arr,seed) RESULT(ans)
      REAL(SP), DIMENSION(:), INTENT(IN) :: arr
      REAL(SP), OPTIONAL, INTENT(IN) :: seed
      REAL(SP), DIMENSION(size(arr)) :: ans
      INTEGER(I4B) :: n,j
      REAL(SP) :: sd
      n=size(arr)
      if (n == 0_i4b) RETURN
      sd=0.0_sp
      if (present(seed)) sd=seed
      ans(1)=arr(1)+sd
      if (n < NPAR_CUMSUM) then
            do j=2,n
                  ans(j)=ans(j-1)+arr(j)
            end do
      else
            ans(2:n:2)=cumsum_r(arr(2:n:2)+arr(1:n-1:2),sd)
            ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
      end if
      END FUNCTION cumsum_r
!BL
      RECURSIVE FUNCTION cumsum_i(arr,seed) RESULT(ans)
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: arr
      INTEGER(I4B), OPTIONAL, INTENT(IN) :: seed
      INTEGER(I4B), DIMENSION(size(arr)) :: ans
      INTEGER(I4B) :: n,j,sd
      n=size(arr)
      if (n == 0_i4b) RETURN
      sd=0_i4b
      if (present(seed)) sd=seed
      ans(1)=arr(1)+sd
      if (n < NPAR_CUMSUM) then
            do j=2,n
                  ans(j)=ans(j-1)+arr(j)
            end do
      else
            ans(2:n:2)=cumsum_i(arr(2:n:2)+arr(1:n-1:2),sd)
            ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
      end if
      END FUNCTION cumsum_i
!BL
!BL
      RECURSIVE FUNCTION cumprod(arr,seed) RESULT(ans)
      REAL(SP), DIMENSION(:), INTENT(IN) :: arr
      REAL(SP), OPTIONAL, INTENT(IN) :: seed
      REAL(SP), DIMENSION(size(arr)) :: ans
      INTEGER(I4B) :: n,j
      REAL(SP) :: sd
      n=size(arr)
      if (n == 0_i4b) RETURN
      sd=1.0_sp
      if (present(seed)) sd=seed
      ans(1)=arr(1)*sd
      if (n < NPAR_CUMPROD) then
            do j=2,n
                  ans(j)=ans(j-1)*arr(j)
            end do
      else
            ans(2:n:2)=cumprod(arr(2:n:2)*arr(1:n-1:2),sd)
            ans(3:n:2)=ans(2:n-1:2)*arr(3:n:2)
      end if
      END FUNCTION cumprod
!BL
!BL
      FUNCTION poly_rr(x,coeffs)
      REAL(SP), INTENT(IN) :: x
      REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs
      REAL(SP) :: poly_rr
      REAL(SP) :: pow
      REAL(SP), DIMENSION(:), ALLOCATABLE :: vec
      INTEGER(I4B) :: i,n,nn
      n=size(coeffs)
      if (n <= 0) then
            poly_rr=0.0_sp
      else if (n < NPAR_POLY) then
            poly_rr=coeffs(n)
            do i=n-1,1,-1
                  poly_rr=x*poly_rr+coeffs(i)
            end do
      else
            allocate(vec(n+1))
            pow=x
            vec(1:n)=coeffs
            do
                  vec(n+1)=0.0_sp
                  nn=ishft(n+1,-1)
                  vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
                  if (nn == 1) exit
                  pow=pow*pow
                  n=nn
            end do
            poly_rr=vec(1)
            deallocate(vec)
      end if
      END FUNCTION poly_rr
!BL
      FUNCTION poly_dd(x,coeffs)
      REAL(DP), INTENT(IN) :: x
      REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs
      REAL(DP) :: poly_dd
      REAL(DP) :: pow
      REAL(DP), DIMENSION(:), ALLOCATABLE :: vec
      INTEGER(I4B) :: i,n,nn
      n=size(coeffs)
      if (n <= 0) then
            poly_dd=0.0_dp
      else if (n < NPAR_POLY) then
            poly_dd=coeffs(n)
            do i=n-1,1,-1
                  poly_dd=x*poly_dd+coeffs(i)
            end do
      else
            allocate(vec(n+1))
            pow=x
            vec(1:n)=coeffs
            do
                  vec(n+1)=0.0_dp
                  nn=ishft(n+1,-1)
                  vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
                  if (nn == 1) exit
                  pow=pow*pow
                  n=nn
            end do
            poly_dd=vec(1)
            deallocate(vec)
      end if
      END FUNCTION poly_dd
!BL
      FUNCTION poly_rc(x,coeffs)
      COMPLEX(SPC), INTENT(IN) :: x
      REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs
      COMPLEX(SPC) :: poly_rc
      COMPLEX(SPC) :: pow
      COMPLEX(SPC), DIMENSION(:), ALLOCATABLE :: vec
      INTEGER(I4B) :: i,n,nn
      n=size(coeffs)
      if (n <= 0) then
            poly_rc=0.0_sp
      else if (n < NPAR_POLY) then
            poly_rc=coeffs(n)
            do i=n-1,1,-1
                  poly_rc=x*poly_rc+coeffs(i)
            end do
      else
            allocate(vec(n+1))
            pow=x
            vec(1:n)=coeffs
            do
                  vec(n+1)=0.0_sp
                  nn=ishft(n+1,-1)
                  vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
                  if (nn == 1) exit
                  pow=pow*pow
                  n=nn
            end do
            poly_rc=vec(1)
            deallocate(vec)
      end if
      END FUNCTION poly_rc
!BL
      FUNCTION poly_cc(x,coeffs)
      COMPLEX(SPC), INTENT(IN) :: x
      COMPLEX(SPC), DIMENSION(:), INTENT(IN) :: coeffs
      COMPLEX(SPC) :: poly_cc
      COMPLEX(SPC) :: pow
      COMPLEX(SPC), DIMENSION(:), ALLOCATABLE :: vec
      INTEGER(I4B) :: i,n,nn
      n=size(coeffs)
      if (n <= 0) then
            poly_cc=0.0_sp
      else if (n < NPAR_POLY) then
            poly_cc=coeffs(n)
            do i=n-1,1,-1
                  poly_cc=x*poly_cc+coeffs(i)
            end do
      else
            allocate(vec(n+1))
            pow=x
            vec(1:n)=coeffs
            do
                  vec(n+1)=0.0_sp
                  nn=ishft(n+1,-1)
                  vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
                  if (nn == 1) exit
                  pow=pow*pow
                  n=nn
            end do
            poly_cc=vec(1)
            deallocate(vec)
      end if
      END FUNCTION poly_cc
!BL
      FUNCTION poly_rrv(x,coeffs)
      REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs,x
      REAL(SP), DIMENSION(size(x)) :: poly_rrv
      INTEGER(I4B) :: i,n,m
      m=size(coeffs)
      n=size(x)
      if (m <= 0) then
            poly_rrv=0.0_sp
      else if (m < n .or. m < NPAR_POLY) then
            poly_rrv=coeffs(m)
            do i=m-1,1,-1
                  poly_rrv=x*poly_rrv+coeffs(i)
            end do
      else
            do i=1,n
                  poly_rrv(i)=poly_rr(x(i),coeffs)
            end do
      end if
      END FUNCTION poly_rrv
!BL
      FUNCTION poly_ddv(x,coeffs)
      REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs,x
      REAL(DP), DIMENSION(size(x)) :: poly_ddv
      INTEGER(I4B) :: i,n,m
      m=size(coeffs)
      n=size(x)
      if (m <= 0) then
            poly_ddv=0.0_dp
      else if (m < n .or. m < NPAR_POLY) then
            poly_ddv=coeffs(m)
            do i=m-1,1,-1
                  poly_ddv=x*poly_ddv+coeffs(i)
            end do
      else
            do i=1,n
                  poly_ddv(i)=poly_dd(x(i),coeffs)
            end do
      end if
      END FUNCTION poly_ddv
!BL
      FUNCTION poly_msk_rrv(x,coeffs,mask)
      REAL(SP), DIMENSION(:), INTENT(IN) :: coeffs,x
      LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
      REAL(SP), DIMENSION(size(x)) :: poly_msk_rrv
      poly_msk_rrv=unpack(poly_rrv(pack(x,mask),coeffs),mask,0.0_sp)
      END FUNCTION poly_msk_rrv
!BL
      FUNCTION poly_msk_ddv(x,coeffs,mask)
      REAL(DP), DIMENSION(:), INTENT(IN) :: coeffs,x
      LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: mask
      REAL(DP), DIMENSION(size(x)) :: poly_msk_ddv
      poly_msk_ddv=unpack(poly_ddv(pack(x,mask),coeffs),mask,0.0_dp)
      END FUNCTION poly_msk_ddv
!BL
!BL
      RECURSIVE FUNCTION poly_term_rr(a,b) RESULT(u)
      REAL(SP), DIMENSION(:), INTENT(IN) :: a
      REAL(SP), INTENT(IN) :: b
      REAL(SP), DIMENSION(size(a)) :: u
      INTEGER(I4B) :: n,j
      n=size(a)
      if (n <= 0) RETURN
      u(1)=a(1)
      if (n < NPAR_POLYTERM) then
            do j=2,n
                  u(j)=a(j)+b*u(j-1)
            end do
      else
            u(2:n:2)=poly_term_rr(a(2:n:2)+a(1:n-1:2)*b,b*b)
            u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
      end if
      END FUNCTION poly_term_rr
!BL
      RECURSIVE FUNCTION poly_term_cc(a,b) RESULT(u)
      COMPLEX(SPC), DIMENSION(:), INTENT(IN) :: a
      COMPLEX(SPC), INTENT(IN) :: b
      COMPLEX(SPC), DIMENSION(size(a)) :: u
      INTEGER(I4B) :: n,j
      n=size(a)
      if (n <= 0) RETURN
      u(1)=a(1)
      if (n < NPAR_POLYTERM) then
            do j=2,n
                  u(j)=a(j)+b*u(j-1)
            end do
      else
            u(2:n:2)=poly_term_cc(a(2:n:2)+a(1:n-1:2)*b,b*b)
            u(3:n:2)=a(3:n:2)+b*u(2:n-1:2)
      end if
      END FUNCTION poly_term_cc
!BL
!BL
      FUNCTION zroots_unity(n,nn)
      INTEGER(I4B), INTENT(IN) :: n,nn
      COMPLEX(SPC), DIMENSION(nn) :: zroots_unity
      INTEGER(I4B) :: k
      REAL(SP) :: theta
      zroots_unity(1)=1.0
      theta=TWOPI/n
      k=1
      do
            if (k >= nn) exit
            zroots_unity(k+1)=cmplx(cos(k*theta),sin(k*theta),SPC)
            zroots_unity(k+2:min(2*k,nn))=zroots_unity(k+1)* &
                        zroots_unity(2:min(k,nn-k))
            k=2*k
      end do
      END FUNCTION zroots_unity
!BL
      FUNCTION outerprod_r(a,b)
      REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(SP), DIMENSION(size(a),size(b)) :: outerprod_r
      outerprod_r = spread(a,dim=2,ncopies=size(b)) * &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerprod_r
!BL
      FUNCTION outerprod_d(a,b)
      REAL(DP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(DP), DIMENSION(size(a),size(b)) :: outerprod_d
      outerprod_d = spread(a,dim=2,ncopies=size(b)) * &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerprod_d
!BL
      FUNCTION outerdiv(a,b)
      REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(SP), DIMENSION(size(a),size(b)) :: outerdiv
      outerdiv = spread(a,dim=2,ncopies=size(b)) / &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerdiv
!BL
      FUNCTION outersum(a,b)
      REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(SP), DIMENSION(size(a),size(b)) :: outersum
      outersum = spread(a,dim=2,ncopies=size(b)) + &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outersum
!BL
      FUNCTION outerdiff_r(a,b)
      REAL(SP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(SP), DIMENSION(size(a),size(b)) :: outerdiff_r
      outerdiff_r = spread(a,dim=2,ncopies=size(b)) - &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerdiff_r
!BL
      FUNCTION outerdiff_d(a,b)
      REAL(DP), DIMENSION(:), INTENT(IN) :: a,b
      REAL(DP), DIMENSION(size(a),size(b)) :: outerdiff_d
      outerdiff_d = spread(a,dim=2,ncopies=size(b)) - &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerdiff_d
!BL
      FUNCTION outerdiff_i(a,b)
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: a,b
      INTEGER(I4B), DIMENSION(size(a),size(b)) :: outerdiff_i
      outerdiff_i = spread(a,dim=2,ncopies=size(b)) - &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerdiff_i
!BL
      FUNCTION outerand(a,b)
      LOGICAL(LGT), DIMENSION(:), INTENT(IN) :: a,b
      LOGICAL(LGT), DIMENSION(size(a),size(b)) :: outerand
      outerand = spread(a,dim=2,ncopies=size(b)) .and. &
                  spread(b,dim=1,ncopies=size(a))
      END FUNCTION outerand
!BL
      SUBROUTINE scatter_add_r(dest,source,dest_index)
      REAL(SP), DIMENSION(:), INTENT(OUT) :: dest
      REAL(SP), DIMENSION(:), INTENT(IN) :: source
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
      INTEGER(I4B) :: m,n,j,i
      n=assert_eq2(size(source),size(dest_index),'scatter_add_r')
      m=size(dest)
      do j=1,n
            i=dest_index(j)
            if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
      end do
      END SUBROUTINE scatter_add_r
      SUBROUTINE scatter_add_d(dest,source,dest_index)
      REAL(DP), DIMENSION(:), INTENT(OUT) :: dest
      REAL(DP), DIMENSION(:), INTENT(IN) :: source
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
      INTEGER(I4B) :: m,n,j,i
      n=assert_eq2(size(source),size(dest_index),'scatter_add_d')
      m=size(dest)
      do j=1,n
            i=dest_index(j)
            if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
      end do
      END SUBROUTINE scatter_add_d
      SUBROUTINE scatter_max_r(dest,source,dest_index)
      REAL(SP), DIMENSION(:), INTENT(OUT) :: dest
      REAL(SP), DIMENSION(:), INTENT(IN) :: source
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
      INTEGER(I4B) :: m,n,j,i
      n=assert_eq2(size(source),size(dest_index),'scatter_max_r')
      m=size(dest)
      do j=1,n
            i=dest_index(j)
            if (i > 0 .and. i <= m) dest(i)=max(dest(i),source(j))
      end do
      END SUBROUTINE scatter_max_r
      SUBROUTINE scatter_max_d(dest,source,dest_index)
      REAL(DP), DIMENSION(:), INTENT(OUT) :: dest
      REAL(DP), DIMENSION(:), INTENT(IN) :: source
      INTEGER(I4B), DIMENSION(:), INTENT(IN) :: dest_index
      INTEGER(I4B) :: m,n,j,i
      n=assert_eq2(size(source),size(dest_index),'scatter_max_d')
      m=size(dest)
      do j=1,n
            i=dest_index(j)
            if (i > 0 .and. i <= m) dest(i)=max(dest(i),source(j))
      end do
      END SUBROUTINE scatter_max_d
!BL
      SUBROUTINE diagadd_rv(mat,diag)
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      REAL(SP), DIMENSION(:), INTENT(IN) :: diag
      INTEGER(I4B) :: j,n
      n = assert_eq2(size(diag),min(size(mat,1),size(mat,2)),'diagadd_rv')
      do j=1,n
            mat(j,j)=mat(j,j)+diag(j)
      end do
      END SUBROUTINE diagadd_rv
!BL
      SUBROUTINE diagadd_r(mat,diag)
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      REAL(SP), INTENT(IN) :: diag
      INTEGER(I4B) :: j,n
      n = min(size(mat,1),size(mat,2))
      do j=1,n
            mat(j,j)=mat(j,j)+diag
      end do
      END SUBROUTINE diagadd_r
!BL
      SUBROUTINE diagmult_rv(mat,diag)
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      REAL(SP), DIMENSION(:), INTENT(IN) :: diag
      INTEGER(I4B) :: j,n
      n = assert_eq2(size(diag),min(size(mat,1),size(mat,2)),'diagmult_rv')
      do j=1,n
            mat(j,j)=mat(j,j)*diag(j)
      end do
      END SUBROUTINE diagmult_rv
!BL
      SUBROUTINE diagmult_r(mat,diag)
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      REAL(SP), INTENT(IN) :: diag
      INTEGER(I4B) :: j,n
      n = min(size(mat,1),size(mat,2))
      do j=1,n
            mat(j,j)=mat(j,j)*diag
      end do
      END SUBROUTINE diagmult_r
!BL
      FUNCTION get_diag_rv(mat)
      REAL(SP), DIMENSION(:,:), INTENT(IN) :: mat
      REAL(SP), DIMENSION(size(mat,1)) :: get_diag_rv
      INTEGER(I4B) :: j
      j=assert_eq2(size(mat,1),size(mat,2),'get_diag_rv')
      do j=1,size(mat,1)
            get_diag_rv(j)=mat(j,j)
      end do
      END FUNCTION get_diag_rv
!BL
      FUNCTION get_diag_dv(mat)
      REAL(DP), DIMENSION(:,:), INTENT(IN) :: mat
      REAL(DP), DIMENSION(size(mat,1)) :: get_diag_dv
      INTEGER(I4B) :: j
      j=assert_eq2(size(mat,1),size(mat,2),'get_diag_dv')
      do j=1,size(mat,1)
            get_diag_dv(j)=mat(j,j)
      end do
      END FUNCTION get_diag_dv
!BL
      SUBROUTINE put_diag_rv(diagv,mat)
      REAL(SP), DIMENSION(:), INTENT(IN) :: diagv
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      INTEGER(I4B) :: j,n
      n=assert_eq2(size(diagv),min(size(mat,1),size(mat,2)),'put_diag_rv')
      do j=1,n
            mat(j,j)=diagv(j)
      end do
      END SUBROUTINE put_diag_rv
!BL
      SUBROUTINE put_diag_r(scal,mat)
      REAL(SP), INTENT(IN) :: scal
      REAL(SP), DIMENSION(:,:), INTENT(INOUT) :: mat
      INTEGER(I4B) :: j,n
      n = min(size(mat,1),size(mat,2))
      do j=1,n
            mat(j,j)=scal
      end do
      END SUBROUTINE put_diag_r
!BL
      SUBROUTINE unit_matrix(mat)
      REAL(SP), DIMENSION(:,:), INTENT(OUT) :: mat
      INTEGER(I4B) :: i,n
      n=min(size(mat,1),size(mat,2))
      mat(:,:)=0.0_sp
      do i=1,n
            mat(i,i)=1.0_sp
      end do
      END SUBROUTINE unit_matrix
!BL
      FUNCTION upper_triangle(j,k,extra)
      INTEGER(I4B), INTENT(IN) :: j,k
      INTEGER(I4B), OPTIONAL, INTENT(IN) :: extra
      LOGICAL(LGT), DIMENSION(j,k) :: upper_triangle
      INTEGER(I4B) :: n
      n=0
      if (present(extra)) n=extra
      upper_triangle=(outerdiff(arth_i(1,1,j),arth_i(1,1,k)) < n)
      END FUNCTION upper_triangle
!BL
      FUNCTION lower_triangle(j,k,extra)
      INTEGER(I4B), INTENT(IN) :: j,k
      INTEGER(I4B), OPTIONAL, INTENT(IN) :: extra
      LOGICAL(LGT), DIMENSION(j,k) :: lower_triangle
      INTEGER(I4B) :: n
      n=0
      if (present(extra)) n=extra
      lower_triangle=(outerdiff(arth_i(1,1,j),arth_i(1,1,k)) > -n)
      END FUNCTION lower_triangle
!BL
      FUNCTION vabs(v)
      REAL(SP), DIMENSION(:), INTENT(IN) :: v
      REAL(SP) :: vabs
      vabs=sqrt(dot_product(v,v))
      END FUNCTION vabs
!BL
      END MODULE nrutil


      MODULE nr
      INTERFACE gasdev
            SUBROUTINE gasdev_s(harvest)
            USE nrtype
            REAL(DP), INTENT(OUT) :: harvest
            END SUBROUTINE gasdev_s
!BL
            SUBROUTINE gasdev_v(harvest)
            USE nrtype
            REAL(DP), DIMENSION(:), INTENT(OUT) :: harvest
            END SUBROUTINE gasdev_v
      END INTERFACE
      INTERFACE
            SUBROUTINE polin2(x1a,x2a,ya,x1,x2,y,dy)
            USE nrtype
            REAL(DP), DIMENSION(:), INTENT(IN) :: x1a,x2a
            REAL(DP), DIMENSION(:,:), INTENT(IN) :: ya
            REAL(DP), INTENT(IN) :: x1,x2
            REAL(DP), INTENT(OUT) :: y,dy
            END SUBROUTINE polin2
      END INTERFACE
      INTERFACE
            SUBROUTINE polint(xa,ya,x,y,dy)
            USE nrtype
            REAL(DP), DIMENSION(:), INTENT(IN) :: xa,ya
            REAL(DP), INTENT(IN) :: x
            REAL(DP), INTENT(OUT) :: y,dy
            END SUBROUTINE polint
      END INTERFACE
      INTERFACE
            FUNCTION ran(idum)
            INTEGER(selected_int_kind(9)), INTENT(INOUT) :: idum
            REAL :: ran
            END FUNCTION ran
      END INTERFACE
      INTERFACE ran0
            SUBROUTINE ran0_s(harvest)
            USE nrtype
            REAL(SP), INTENT(OUT) :: harvest
            END SUBROUTINE ran0_s
!BL
            SUBROUTINE ran0_v(harvest)
            USE nrtype
            REAL(SP), DIMENSION(:), INTENT(OUT) :: harvest
            END SUBROUTINE ran0_v
      END INTERFACE
      INTERFACE ran1
            SUBROUTINE ran1_s(harvest)
            USE nrtype
            REAL(DP), INTENT(OUT) :: harvest
            END SUBROUTINE ran1_s
!BL
            SUBROUTINE ran1_v(harvest)
            USE nrtype
            REAL(DP), DIMENSION(:), INTENT(OUT) :: harvest
            END SUBROUTINE ran1_v
      END INTERFACE
      INTERFACE ran2
            SUBROUTINE ran2_s(harvest)
            USE nrtype
            REAL(SP), INTENT(OUT) :: harvest
            END SUBROUTINE ran2_s
!BL
            SUBROUTINE ran2_v(harvest)
            USE nrtype
            REAL(SP), DIMENSION(:), INTENT(OUT) :: harvest
            END SUBROUTINE ran2_v
      END INTERFACE
      INTERFACE ran3
            SUBROUTINE ran3_s(harvest)
            USE nrtype
            REAL(SP), INTENT(OUT) :: harvest
            END SUBROUTINE ran3_s
!BL
            SUBROUTINE ran3_v(harvest)
            USE nrtype
            REAL(SP), DIMENSION(:), INTENT(OUT) :: harvest
            END SUBROUTINE ran3_v
      END INTERFACE
      END MODULE nr

      MODULE ran_state
      USE nrtype
      IMPLICIT NONE
      INTEGER, PARAMETER :: K4B=selected_int_kind(9)
      INTEGER(K4B), PARAMETER :: hg=huge(1_K4B), hgm=-hg, hgng=hgm-1
      INTEGER(K4B), SAVE :: lenran=0, seq=0
      INTEGER(K4B), SAVE :: iran0,jran0,kran0,nran0,mran0,rans
      INTEGER(K4B), DIMENSION(:,:), POINTER, SAVE :: ranseeds
      INTEGER(K4B), DIMENSION(:), POINTER, SAVE :: iran,jran,kran, &
                  nran,mran,ranv
      REAL(SP), SAVE :: amm
      INTERFACE ran_hash
            MODULE PROCEDURE ran_hash_s, ran_hash_v
      END INTERFACE
      CONTAINS
!BL
      SUBROUTINE ran_init(length)
      USE nrtype; USE nrutil, ONLY : arth,nrerror,reallocate
      IMPLICIT NONE
      INTEGER(K4B), INTENT(IN) :: length
      INTEGER(K4B) :: new,j,hgt
      if (length < lenran) RETURN
      hgt=hg
      if (hg /= 2147483647) call nrerror('ran_init: arith assump 1 fails')
      if (hgng >= 0)        call nrerror('ran_init: arith assump 2 fails')
!       if (hgt+1 /= hgng)    call nrerror('ran_init: arith assump 3 fails')
      if (not(hg) >= 0)     call nrerror('ran_init: arith assump 4 fails')
      if (not(hgng) < 0)    call nrerror('ran_init: arith assump 5 fails')
      if (hg+hgng >= 0)     call nrerror('ran_init: arith assump 6 fails')
      if (not(-1_k4b) < 0)  call nrerror('ran_init: arith assump 7 fails')
      if (not(0_k4b) >= 0)  call nrerror('ran_init: arith assump 8 fails')
      if (not(1_k4b) >= 0)  call nrerror('ran_init: arith assump 9 fails')
      if (lenran > 0) then
            ranseeds=>reallocate(ranseeds,length,5)
            ranv=>reallocate(ranv,length-1)
            new=lenran+1
      else
            allocate(ranseeds(length,5))
            allocate(ranv(length-1))
            new=1
            amm=nearest(1.0_sp,-1.0_sp)/hgng
            if (amm*hgng >= 1.0 .or. amm*hgng <= 0.0) &
                        call nrerror('ran_init: arth assump 10 fails')
      end if
      ranseeds(new:,1)=seq
      ranseeds(new:,2:5)=spread(arth(new,1,size(ranseeds(new:,1))),2,4)
      do j=1,4
            call ran_hash(ranseeds(new:,j),ranseeds(new:,j+1))
      end do
      where (ranseeds(new:,1:3) < 0) &
                  ranseeds(new:,1:3)=not(ranseeds(new:,1:3))
      where (ranseeds(new:,4:5) == 0) ranseeds(new:,4:5)=1
      if (new == 1) then
            iran0=ranseeds(1,1)
            jran0=ranseeds(1,2)
            kran0=ranseeds(1,3)
            mran0=ranseeds(1,4)
            nran0=ranseeds(1,5)
            rans=nran0
      end if
      if (length > 1) then
            iran => ranseeds(2:,1)
            jran => ranseeds(2:,2)
            kran => ranseeds(2:,3)
            mran => ranseeds(2:,4)
            nran => ranseeds(2:,5)
            ranv = nran
      end if
      lenran=length
      END SUBROUTINE ran_init
!BL
      SUBROUTINE ran_deallocate
      if (lenran > 0) then
            deallocate(ranseeds,ranv)
            nullify(ranseeds,ranv,iran,jran,kran,mran,nran)
            lenran = 0
      end if
      END SUBROUTINE ran_deallocate
!BL
      SUBROUTINE ran_seed(sequence,size,put,get)
      IMPLICIT NONE
      INTEGER, OPTIONAL, INTENT(IN) :: sequence
      INTEGER, OPTIONAL, INTENT(OUT) :: size
      INTEGER, DIMENSION(:), OPTIONAL, INTENT(IN) :: put
      INTEGER, DIMENSION(:), OPTIONAL, INTENT(OUT) :: get
      if (present(size)) then
            size=5*lenran
      else if (present(put)) then
            if (lenran == 0) RETURN
            ranseeds=reshape(put,shape(ranseeds))
            where (ranseeds(:,1:3) < 0) ranseeds(:,1:3)=not(ranseeds(:,1:3))
            where (ranseeds(:,4:5) == 0) ranseeds(:,4:5)=1
            iran0=ranseeds(1,1)
            jran0=ranseeds(1,2)
            kran0=ranseeds(1,3)
            mran0=ranseeds(1,4)
            nran0=ranseeds(1,5)
      else if (present(get)) then
            if (lenran == 0) RETURN
            ranseeds(1,1:5)=(/ iran0,jran0,kran0,mran0,nran0 /)
            get=reshape(ranseeds,shape(get))
      else if (present(sequence)) then
            call ran_deallocate
            seq=sequence
      end if
      END SUBROUTINE ran_seed
!BL
      SUBROUTINE ran_hash_s(il,ir)
      IMPLICIT NONE
      INTEGER(K4B), INTENT(INOUT) :: il,ir
      INTEGER(K4B) :: is,j
      do j=1,4
            is=ir
            ir=ieor(ir,ishft(ir,5))+1422217823
            ir=ieor(ir,ishft(ir,-16))+1842055030
            ir=ieor(ir,ishft(ir,9))+80567781
            ir=ieor(il,ir)
            il=is
      end do
      END SUBROUTINE ran_hash_s
!BL
      SUBROUTINE ran_hash_v(il,ir)
      IMPLICIT NONE
      INTEGER(K4B), DIMENSION(:), INTENT(INOUT) :: il,ir
      INTEGER(K4B), DIMENSION(size(il)) :: is
      INTEGER(K4B) :: j
      do j=1,4
            is=ir
            ir=ieor(ir,ishft(ir,5))+1422217823
            ir=ieor(ir,ishft(ir,-16))+1842055030
            ir=ieor(ir,ishft(ir,9))+80567781
            ir=ieor(il,ir)
            il=is
      end do
      END SUBROUTINE ran_hash_v
      END MODULE ran_state

      SUBROUTINE ran1_s(harvest)
      USE nrtype
      USE ran_state, ONLY: K4B,amm,lenran,ran_init, &
                  iran0,jran0,kran0,nran0,mran0,rans
      IMPLICIT NONE
      REAL(DP), INTENT(OUT) :: harvest
      if (lenran < 1) call ran_init(1)
      rans=iran0-kran0
      if (rans < 0) rans=rans+2147483579_k4b
      iran0=jran0
      jran0=kran0
      kran0=rans
      nran0=ieor(nran0,ishft(nran0,13))
      nran0=ieor(nran0,ishft(nran0,-17))
      nran0=ieor(nran0,ishft(nran0,5))
      if (nran0 == 1) nran0=270369_k4b
      mran0=ieor(mran0,ishft(mran0,5))
      mran0=ieor(mran0,ishft(mran0,-13))
      mran0=ieor(mran0,ishft(mran0,6))
      rans=ieor(nran0,rans)+mran0
      harvest=amm*merge(rans,not(rans), rans<0 )
      END SUBROUTINE ran1_s

      SUBROUTINE ran1_v(harvest)
      USE nrtype
      USE ran_state, ONLY: K4B,amm,lenran,ran_init, &
                  iran,jran,kran,nran,mran,ranv
      IMPLICIT NONE
      REAL(DP), DIMENSION(:), INTENT(OUT) :: harvest
      INTEGER(K4B) :: n
      n=size(harvest)
      if (lenran < n+1) call ran_init(n+1)
      ranv(1:n)=iran(1:n)-kran(1:n)
      where (ranv(1:n) < 0) ranv(1:n)=ranv(1:n)+2147483579_k4b
      iran(1:n)=jran(1:n)
      jran(1:n)=kran(1:n)
      kran(1:n)=ranv(1:n)
      nran(1:n)=ieor(nran(1:n),ishft(nran(1:n),13))
      nran(1:n)=ieor(nran(1:n),ishft(nran(1:n),-17))
      nran(1:n)=ieor(nran(1:n),ishft(nran(1:n),5))
      where (nran(1:n) == 1) nran(1:n)=270369_k4b
      mran(1:n)=ieor(mran(1:n),ishft(mran(1:n),5))
      mran(1:n)=ieor(mran(1:n),ishft(mran(1:n),-13))
      mran(1:n)=ieor(mran(1:n),ishft(mran(1:n),6))
      ranv(1:n)=ieor(nran(1:n),ranv(1:n))+mran(1:n)
      harvest=amm*merge(ranv(1:n),not(ranv(1:n)), ranv(1:n)<0 )
      END SUBROUTINE ran1_v

      SUBROUTINE gasdev_s(harvest)
      USE nrtype
      USE nr, ONLY : ran1
      IMPLICIT NONE
      REAL(DP), INTENT(OUT) :: harvest
      REAL(DP) :: rsq,v1,v2
      REAL(DP), SAVE :: g
      LOGICAL, SAVE :: gaus_stored=.false.
      if (gaus_stored) then
            harvest=g
            gaus_stored=.false.
      else
            do
                  call ran1(v1)
                  call ran1(v2)
                  v1=2.0_sp*v1-1.0_sp
                  v2=2.0_sp*v2-1.0_sp
                  rsq=v1**2+v2**2
                  if (rsq > 0.0 .and. rsq < 1.0) exit
            end do
            rsq=sqrt(-2.0_sp*log(rsq)/rsq)
            harvest=v1*rsq
            g=v2*rsq
            gaus_stored=.true.
      end if
      END SUBROUTINE gasdev_s

      SUBROUTINE gasdev_v(harvest)
      USE nrtype; USE nrutil, ONLY : array_copy
      USE nr, ONLY : ran1
      IMPLICIT NONE
      REAL(DP), DIMENSION(:), INTENT(OUT) :: harvest
      REAL(DP), DIMENSION(size(harvest)) :: rsq,v1,v2
      REAL(DP), ALLOCATABLE, DIMENSION(:), SAVE :: g
      INTEGER(I4B) :: n,ng,nn,m
      INTEGER(I4B), SAVE :: last_allocated=0
      LOGICAL, SAVE :: gaus_stored=.false.
      LOGICAL, DIMENSION(size(harvest)) :: mask
      n=size(harvest)
      if (n /= last_allocated) then
            if (last_allocated /= 0) deallocate(g)
            allocate(g(n))
            last_allocated=n
            gaus_stored=.false.
      end if
      if (gaus_stored) then
            harvest=g
            gaus_stored=.false.
      else
            ng=1
            do
                  if (ng > n) exit
                  call ran1(v1(ng:n))
                  call ran1(v2(ng:n))
                  v1(ng:n)=2.0_sp*v1(ng:n)-1.0_sp
                  v2(ng:n)=2.0_sp*v2(ng:n)-1.0_sp
                  rsq(ng:n)=v1(ng:n)**2+v2(ng:n)**2
                  mask(ng:n)=(rsq(ng:n)>0.0 .and. rsq(ng:n)<1.0)
                  call array_copy(pack(v1(ng:n),mask(ng:n)),v1(ng:),nn,m)
                  v2(ng:ng+nn-1)=pack(v2(ng:n),mask(ng:n))
                  rsq(ng:ng+nn-1)=pack(rsq(ng:n),mask(ng:n))
                  ng=ng+nn
            end do
            rsq=sqrt(-2.0_sp*log(rsq)/rsq)
            harvest=v1*rsq
            g=v2*rsq
            gaus_stored=.true.
      end if
      END SUBROUTINE gasdev_v

	MODULE randomNum
		USE nr 
		IMPLICIT NONE

		CONTAINS
		SUBROUTINE preIntRan()
			USE ran_state
			IMPLICIT NONE
			INTEGER :: clock,seed
			! Initialize random number generator
			CALL SYSTEM_CLOCK(COUNT=clock)
			seed=clock
        	CALL ran_seed(sequence=seed)

! 			write(0,*) "Seed",seed
		END SUBROUTINE
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		DOUBLE PRECISION FUNCTION unif(a,b)
		
		! Generate a random number using a uniform distribution on the
		! interval [a,b]
		! Bart Willems, 22/03/01
			IMPLICIT NONE
			DOUBLE PRECISION, INTENT(IN) :: a,b
			DOUBLE PRECISION :: x
			
			! Generate random x uniformly distributed on [0,1]
			CALL ran1(x)
			
			! Transform into uniform distribution on [a,b]
			unif = a+x*(b-a)
			
			RETURN
		END FUNCTION unif
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		DOUBLE PRECISION FUNCTION gaussDraw(mean,sigma)
			IMPLICIT NONE
! 			DOUBLE PRECISION,INTENT(IN) :: mean,sigma
! 			DOUBLE PRECISION :: x,y,z
! 
! 			y=0.d0
! 			z=1.d0
! 
! 			DO WHILE (z .gt. y)
! 				call ran1(x)
! 				call ran1(z)
! 				y=0.5*(1.d0+erf((x-mean)/sqrt(2.d0*sigma)))
! !  				write(*,*) x,y,z
! 			END DO

 			DOUBLE PRECISION,INTENT(IN) :: mean,sigma
 			DOUBLE PRECISION :: x1,x2,r2

			r2=0.d0

			DO WHILE (r2>=1.d0 .or. r2==0.d0)
				CALL ran1(x1)
				CALL ran1(x2)
           		x1 = 2.0*x1 - 1.0;
            	x2 = 2.0*x2 - 1.0;
            	r2 = x1*x1 + x2*x2;
			END DO

        ! Box-Muller transform 
			gaussDraw = sqrt(-2.0*log(r2)/r2)*x2

			gaussDraw=mean+gaussDraw*sigma
		END FUNCTION gaussDraw

	END MODULE randomNum
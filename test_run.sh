#!/bin/bash

# setup input variables
VERSION=1


# setup paths
CODE_DIR="/padata/beta/users/efarrell/repos/biseps"
INPUT_DIR="./input/pop_b_o/1"
OUTPUT_DIR="./test_output"


# Create output folder if
# it doesnt exist already
if [ ! -d "$OUTPUT_DIR" ]; then
    mkdir "$OUTPUT_DIR"
fi


# get utility functions.
# This file is important and contains 
# bash functions used throughout this script.
source "$CODE_DIR/common/utility_functions.sh"


# Setup Python runtime environment
set_python_env  anaconda  $CODE_DIR



# input files
EXTRACT_FILE="$INPUT_DIR/extract.onefile"
# EXTRACT_FILE="$INPUT_DIR/extract.$VERSION"

# output files
COEFF_FILE="$OUTPUT_DIR/coeff.$VERSION"
DEPTHS_FILE="$OUTPUT_DIR/eclipse_depth.$VERSION"


# check that required folders and files exist
assert_folder_exists    "OUTPUT_DIR"    "$OUTPUT_DIR"
assert_file_exists      "EXTRACT_FILE"  "$EXTRACT_FILE"


# First create gravity and limb 
# darkening coefficients
printf "\nstarting...  magLdcGd.py"
$PYTHONBIN $CODE_DIR/mag/magLdcGd.py  $EXTRACT_FILE  > "$COEFF_FILE"
printf "\nmagLdcGd.py finished"


# Now double-check a coefficient 
# file was actually created
assert_file_exists  "COEFF_FILE"    "$COEFF_FILE"


# Finally get transit depths using JKTEBOP
printf "\n\nstarting...  jktebopGen"
./jktebopGen  $EXTRACT_FILE  $COEFF_FILE   > "$DEPTHS_FILE"
printf "\njktebopGen finished"


